/*
 * ideviceinstall2: Clone of ideviceinstall using libgimd
 *
 * Copyright (C) 2015 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Author: Christophe Fergeau <cfergeau@redhat.com>
 */
#include <glib.h>
#include <libgimd/gimd-application.h>
#include <libgimd/gimd-device.h>


typedef struct _AsyncClosure {
    GMainLoop *main_loop;
} AsyncClosure;

typedef struct {
    gboolean set;
    char *value;
} IdeviceInstallOptionCommand;

typedef struct {
    char *udid;
    gboolean list_apps;
    gboolean list_archives;
    IdeviceInstallOptionCommand install;
    IdeviceInstallOptionCommand uninstall;
    IdeviceInstallOptionCommand upgrade;
    IdeviceInstallOptionCommand archive;
    IdeviceInstallOptionCommand restore;
    IdeviceInstallOptionCommand remove_archive;
    char *options;
    gboolean debug;
} IdeviceInstallCmdlineOptions;

IdeviceInstallCmdlineOptions parsed_options;

static gboolean parse_option_command(const gchar *option_name, const gchar *value,
                                     gpointer data, GError **error)
{
    IdeviceInstallCmdlineOptions *options = data;

    if ((g_strcmp0(option_name, "--install") == 0) ||
       (g_strcmp0(option_name, "-i") == 0)) {
        options->install.set = TRUE;
        options->install.value = g_strdup(value);
    } else if ((g_strcmp0(option_name, "uninstall") == 0) ||
               (g_strcmp0(option_name, "-U") == 0)) {
        options->uninstall.set = TRUE;
        options->uninstall.value = g_strdup(value);
    } else if ((g_strcmp0(option_name, "upgrade") == 0) ||
               (g_strcmp0(option_name, "-g") == 0)) {
        options->upgrade.set = TRUE;
        options->upgrade.value = g_strdup(value);
    } else if ((g_strcmp0(option_name, "archive") == 0) ||
               (g_strcmp0(option_name, "-a") == 0)) {
        options->archive.set = TRUE;
        options->archive.value = g_strdup(value);
    } else if ((g_strcmp0(option_name, "restore") == 0) ||
               (g_strcmp0(option_name, "-r") == 0)) {
        options->restore.set = TRUE;
        options->restore.value = g_strdup(value);
    } else if ((g_strcmp0(option_name, "remove-archive") == 0) ||
               (g_strcmp0(option_name, "-R") == 0)) {
        options->remove_archive.set = TRUE;
        options->remove_archive.value = g_strdup(value);
    } else {
        g_warn_if_reached();
        g_set_error(error, G_OPTION_ERROR, G_OPTION_ERROR_FAILED,
                    "Unknow option: %s", option_name);
        return FALSE;
    }
    return TRUE;
}

static GOptionEntry common_options[] = {
    { "udid",           'u', 0, G_OPTION_ARG_STRING,   &parsed_options.udid,          "Target specific device by its 40-digit device UDID.", "UDID" },
    { "list-apps",      'l', 0, G_OPTION_ARG_NONE,     &parsed_options.list_apps,     "List applications.", NULL },
    { "list-archives",  'L', 0, G_OPTION_ARG_NONE,     &parsed_options.list_archives, "List archived applications.", NULL },
    { "install",        'i', 0, G_OPTION_ARG_CALLBACK, parse_option_command,          "Install app from package file specified by ARCHIVE. ARCHIVE can also be a .ipcc file for carrier bundles.", "ARCHIVE" },
    { "uninstall",      'U', 0, G_OPTION_ARG_CALLBACK, parse_option_command,          "Uninstall app specified by APPID.", "APPID" },
    { "upgrade",        'g', 0, G_OPTION_ARG_CALLBACK, parse_option_command,          "Upgrade app from package file specified by ARCHIVE.", "ARCHIVE" },
    { "archive",        'a', 0, G_OPTION_ARG_CALLBACK, parse_option_command,          "Archive app specified by APPID.", "APPID" },
    { "restore",        'r', 0, G_OPTION_ARG_CALLBACK, parse_option_command,          "Restore archived app specified by APPID.", "APPID" },
    { "remove-archive", 'R', 0, G_OPTION_ARG_CALLBACK, parse_option_command,          "Remove app archive specified by APPID.", "APPID" },
    { "options",        'o', 0, G_OPTION_ARG_STRING,   &parsed_options.options,       "Pass additional options to the specified command.", NULL  },
    { "debug",          'd', 0, G_OPTION_ARG_NONE,     &parsed_options.debug,         "Enable communication debugging.", NULL },
    { NULL,             0,   0, G_OPTION_ARG_NONE,     NULL,                           NULL, NULL }
};


static gboolean parse_cmdline(int *argc, char ***argv)
{
    GOptionContext *context;
    GOptionGroup *group;
    GError *error = NULL;
    gboolean parsed;

    context = g_option_context_new(" - Manage apps on iOS devices.");
    group = g_option_group_new(NULL, NULL, NULL, &parsed_options, NULL);
    g_option_group_add_entries(group, common_options);
    g_option_context_set_main_group(context, group);
    parsed = g_option_context_parse(context, argc, argv, &error);
    g_option_context_free(context);
    if (!parsed) {
        g_warning("failed to parse options: %s", error->message);
    }
    return parsed;
}


static void print_gimd_application(GimdApplication *application)
{
    char *appid;
    char *appname;

    g_object_get(G_OBJECT(application),
                 "appid", &appid,
                 "display-name", &appname,
                 NULL);
    g_print("  %s - %s\n", appid, appname);
    g_free(appid);
    g_free(appname);
}


static void real_list_apps_done(GObject *source_object,
                                GList *(*list_finish)(GimdDevice *device, GAsyncResult *result, GError **error),
                                GAsyncResult *result,
                                gpointer user_data)
{
    GimdDevice *device;
    AsyncClosure *closure = user_data;
    GError *error = NULL;
    GList *apps = NULL;
    GList *it;

    device = GIMD_DEVICE(source_object);
    apps = list_finish(device, result, &error);
    if (error != NULL) {
        g_warning("Failed to list applications: %s", error->message);
        goto end;
    }
    for (it = apps; it != NULL; it = it->next) {
        print_gimd_application(GIMD_APPLICATION(it->data));
    }

end:
    g_list_free_full(apps, g_object_unref);
    g_main_loop_quit(closure->main_loop);
}

static void list_apps_done(GObject *source_object,
                           GAsyncResult *result,
                           gpointer user_data)
{
    real_list_apps_done(source_object, gimd_device_list_applications_finish,
                        result, user_data);
}

static void handle_list_apps(GimdDevice *device, AsyncClosure *closure)
{
    gimd_device_list_applications(device, 0, NULL,
                                  list_apps_done, closure);
}


static void list_archives_done(GObject *source_object,
                               GAsyncResult *result,
                               gpointer user_data)
{
    real_list_apps_done(source_object,
                        gimd_device_list_archived_applications_finish,
                        result, user_data);
}

static void handle_list_archives(GimdDevice *device, AsyncClosure *closure)
{
    gimd_device_list_archived_applications(device, 0, NULL,
                                           list_archives_done, closure);
}


static void install_done(GObject *source_object,
                         GAsyncResult *result,
                         gpointer user_data)
{
    GimdDevice *device;
    AsyncClosure *closure = user_data;
    GError *error = NULL;

    device = GIMD_DEVICE(source_object);
    gimd_device_install_application_finish(device, result, &error);
    if (error != NULL) {
        g_warning("Failed to install application bundle: %s", error->message);
        g_error_free(error);
    }
    g_main_loop_quit(closure->main_loop);
}

static void handle_install(GimdDevice *device, const char *archive,
                           AsyncClosure *closure)
{
    GFile *bundle = g_file_new_for_path(archive);

    gimd_device_install_application(device, bundle, NULL, NULL, NULL, install_done, closure);
    g_object_unref(bundle);
}


static void uninstall_done(GObject *source_object,
                           GAsyncResult *result,
                           gpointer user_data)
{
    GimdApplication *app;
    AsyncClosure *closure = user_data;
    GError *error = NULL;

    app = GIMD_APPLICATION(source_object);
    gimd_application_uninstall_finish(app, result, &error);
    if (error != NULL) {
        g_warning("Failed to uninstall application: %s", error->message);
        g_error_free(error);
    }
    g_main_loop_quit(closure->main_loop);
}

static void handle_uninstall(GimdDevice *device, const char *appid,
                             AsyncClosure *closure)
{
    GimdApplication *app;

    app = gimd_application_new(appid);
    g_object_set(app, "device", device, NULL);
    gimd_application_uninstall(app, 0, NULL, NULL, NULL,
                               uninstall_done, closure);
    g_object_unref(app);
}


static void upgrade_done(GObject *source_object,
                         GAsyncResult *result,
                         gpointer user_data)
{
    GimdDevice *device;
    AsyncClosure *closure = user_data;
    GError *error = NULL;

    device = GIMD_DEVICE(source_object);
    gimd_device_upgrade_application_finish(device, result, &error);
    if (error != NULL) {
        g_warning("Failed to upgrade application bundle: %s", error->message);
        g_error_free(error);
    }
    g_main_loop_quit(closure->main_loop);
}

static void handle_upgrade(GimdDevice *device, const char *archive,
                           AsyncClosure *closure)
{
    GFile *bundle = g_file_new_for_path(archive);

    gimd_device_upgrade_application(device, bundle, NULL, NULL, NULL,
                                    upgrade_done, closure);
    g_object_unref(bundle);
}


static void archive_done(GObject *source_object,
                         GAsyncResult *result,
                         gpointer user_data)
{
    GimdApplication *app;
    AsyncClosure *closure = user_data;
    GError *error = NULL;

    app = GIMD_APPLICATION(source_object);
    gimd_application_archive_finish(app, result, &error);
    if (error != NULL) {
        g_warning("Failed to archive application: %s", error->message);
        g_error_free(error);
    }
    g_main_loop_quit(closure->main_loop);
}

static void handle_archive(GimdDevice *device, const char *appid,
                           AsyncClosure *closure)
{
    GimdApplication *app;

    app = gimd_application_new(appid);
    g_object_set(app, "device", device, NULL);
    gimd_application_archive(app, 0, NULL, NULL, NULL,
                             archive_done, closure);
    g_object_unref(app);
}


static void restore_archive_done(GObject *source_object,
                                 GAsyncResult *result,
                                 gpointer user_data)
{
    GimdApplication *app;
    AsyncClosure *closure = user_data;
    GError *error = NULL;

    app = GIMD_APPLICATION(source_object);
    gimd_application_restore_archive_finish(app, result, &error);
    if (error != NULL) {
        g_warning("Failed to restore archived application: %s", error->message);
        g_error_free(error);
    }
    g_main_loop_quit(closure->main_loop);
}

static void handle_restore(GimdDevice *device, const char *appid,
                           AsyncClosure *closure)
{
    GimdApplication *app;

    app = gimd_application_new(appid);
    g_object_set(app, "device", device, NULL);
    gimd_application_restore_archive(app, 0, NULL, NULL, NULL,
                                     restore_archive_done, closure);
    g_object_unref(app);
}


static void remove_archive_done(GObject *source_object,
                                GAsyncResult *result,
                                gpointer user_data)
{
    GimdApplication *app;
    AsyncClosure *closure = user_data;
    GError *error = NULL;

    app = GIMD_APPLICATION(source_object);
    gimd_application_remove_archive_finish(app, result, &error);
    if (error != NULL) {
        g_warning("Failed to remove archived application: %s", error->message);
        g_error_free(error);
    }
    g_main_loop_quit(closure->main_loop);
}

static void handle_remove_archive(GimdDevice *device, const char *appid,
                                  AsyncClosure *closure)
{
    GimdApplication *app;

    app = gimd_application_new(appid);
    g_object_set(app, "device", device, NULL);
    gimd_application_remove_archive(app, 0, NULL, NULL, NULL,
                                    remove_archive_done, closure);
    g_object_unref(app);
}


int main(int argc, char **argv)
{
    GimdDevice *device;
    AsyncClosure closure = { 0, };

    closure.main_loop = g_main_loop_new(NULL, FALSE);

    if (!parse_cmdline(&argc, &argv)) {
        return 1;
    }
    if (!parsed_options.udid) {
        g_warning("Device UDID is mandatory");
        return 1;
    }
    device = gimd_device_new(parsed_options.udid);
    g_assert(device != NULL);

    if (parsed_options.options) {
        g_print("options: %s\n", parsed_options.options);
    }
    if (parsed_options.debug) {
        g_print("debug\n");
    }

    if (parsed_options.list_apps) {
        handle_list_apps(device, &closure);
    } else if (parsed_options.list_archives) {
        handle_list_archives(device, &closure);
    } else if (parsed_options.install.set) {
        handle_install(device, parsed_options.install.value, &closure);
    } else if (parsed_options.uninstall.set) {
        handle_uninstall(device, parsed_options.uninstall.value, &closure);
    } else if (parsed_options.upgrade.set) {
        handle_upgrade(device, parsed_options.upgrade.value, &closure);
    } else if (parsed_options.archive.set) {
        handle_archive(device, parsed_options.archive.value, &closure);
    } else if (parsed_options.restore.set) {
        handle_restore(device, parsed_options.restore.value, &closure);
    } else if (parsed_options.remove_archive.set) {
        handle_remove_archive(device, parsed_options.remove_archive.value,
                              &closure);
    }
    g_object_unref(device);

    g_main_loop_run(closure.main_loop);
    g_main_loop_unref(closure.main_loop);

    return 0;
}
