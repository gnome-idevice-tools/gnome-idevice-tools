/*
 * test-list-apps.c: simple test for gimd_device_archive_applications()
 *
 * Copyright (C) 2014, 2015 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Author: Christophe Fergeau <cfergeau@redhat.com>
 */
#include <config.h>

#include <libgimd/gimd-application.h>
#include <libgimd/gimd-device.h>


typedef struct {
    const char *appid;
    const char *device_udid;
    GMainLoop *main_loop;
} TestData;


static void application_archived_cb(GObject *source_object,
                                    GAsyncResult *result,
                                    gpointer user_data)
{
    TestData *test_data = user_data;
    GimdApplication *app = GIMD_APPLICATION(source_object);
    GError *error = NULL;
    gboolean archived;

    archived = gimd_application_archive_finish(app, result, &error);
    if (!archived) {
        g_warn_if_fail(error != NULL);
        g_print("Could not archive application: %s", error->message);
        g_clear_error(&error);
        g_main_loop_quit(test_data->main_loop);
        return;
    }

    g_main_loop_quit(test_data->main_loop);
}


static void list_applications_cb(GObject *source_object,
                                 GAsyncResult *result,
                                 gpointer user_data)
{
    TestData *test_data = user_data;
    GimdDevice *device = GIMD_DEVICE(source_object);
    GError *error = NULL;
    GList *apps;
    GimdApplication *app;
    GList *it;

    apps = gimd_device_list_applications_finish(device, result, &error);
    if (apps == NULL) {
        g_warn_if_fail(error != NULL);
        g_print("Could not get application list: %s", error->message);
        g_clear_error(&error);
        g_main_loop_quit(test_data->main_loop);
        return;
    }

    app = NULL;
    for (it = apps; it != NULL; it = it->next) {
        char *appid;

        g_assert(GIMD_IS_APPLICATION(it->data));
        g_object_get(G_OBJECT(it->data),
                     "appid", &appid,
                     NULL);
        if (g_strcmp0(appid, test_data->appid) == 0) {
            app = g_object_ref(it->data);
        }
        g_free(appid);
    }
    g_list_free_full(apps, g_object_unref);

    if (app == NULL) {
        g_print("Could not find application '%s'", test_data->appid);
        /* EXIT */
        g_assert_not_reached();
    }

    gimd_application_archive(app, 0, NULL,
                             NULL, NULL,
                             application_archived_cb, test_data);
    g_object_unref(G_OBJECT(app));
}

static gboolean real_main(gpointer user_data)
{
    TestData *test_data = user_data;
    GimdDevice *device;

    device = gimd_device_new(test_data->device_udid);
    g_assert(device != NULL);

    gimd_device_list_applications(device, 0, NULL,
                                  list_applications_cb,
                                  test_data);
    g_object_unref(G_OBJECT(device));

    return G_SOURCE_REMOVE;
}


int main(int argc, char **argv)
{
    TestData test_data;

    if (argc != 3) {
        g_print("Device udid andd app-id must be passed as the only 2 arguments\n");
        return 1;
    }

    test_data.device_udid = argv[1];
    test_data.appid = argv[2];
    test_data.main_loop = g_main_loop_new(NULL, FALSE);
    g_idle_add(real_main, &test_data);

    g_main_loop_run(test_data.main_loop);
    g_main_loop_unref(test_data.main_loop);

    return 0;
}

