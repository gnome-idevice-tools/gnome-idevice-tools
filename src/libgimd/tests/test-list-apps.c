/*
 * test-list-apps.c: simple test for gimd_device_list_applications()
 *
 * Copyright (C) 2014 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Author: Christophe Fergeau <cfergeau@redhat.com>
 */
#include <config.h>

#include <libgimd/gimd-application.h>
#include <libgimd/gimd-device.h>

int main(int argc, char **argv)
{
    GimdDevice *device;
    GList *apps;
    GList *it;

    if (argc != 2) {
        g_print("Device udid must be passed as the only argument\n");
        return 1;
    }

    device = gimd_device_new(argv[1]);
    g_assert(device != NULL);

    apps = gimd_device_list_applications_sync(device, 0, NULL);
    if (apps == NULL) {
        g_print("No applications, communication with device failed?\n");
        g_object_unref(G_OBJECT(device));
        return 2;
    }

    g_print("Applications:\n");
    for (it = apps; it != NULL; it = it->next) {
        char *appid;
        char *appname;

        g_assert(GIMD_IS_APPLICATION(it->data));
        g_object_get(G_OBJECT(it->data),
                     "appid", &appid,
                     "display-name", &appname,
                     NULL);
        g_print("  %s - %s\n", appid, appname);
        g_free(appid);
        g_free(appname);
    }

    g_list_free_full(apps, g_object_unref);
    g_object_unref(G_OBJECT(device));

    return 0;
}

