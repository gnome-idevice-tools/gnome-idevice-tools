/*
 * ideviceinfo2: Clone of idevice_id/ideviceinfo using libgimd
 *
 * Copyright (C) 2015 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Author: Christophe Fergeau <cfergeau@redhat.com>
 */
#include <glib.h>
#include <libgimd/gimd-device.h>
#include <libgimd/gimd-device-monitor.h>
#include <stdlib.h>

typedef struct {
    char *udid;
    gboolean list_devices;
    gboolean watch_devices;
    gboolean show_info;
    char *domain;
    char *key;
    gboolean xml;
} IdeviceInfoCmdlineOptions;

IdeviceInfoCmdlineOptions parsed_options;

static GOptionEntry common_options[] = {
    { "udid",           'u', 0, G_OPTION_ARG_STRING,   &parsed_options.udid,          "Target specific device by its 40-digit device UDID.", "UDID" },
    { "list-devices",   'l', 0, G_OPTION_ARG_NONE,     &parsed_options.list_devices,  "List connected devices.", NULL },
    { "info",           'i', 0, G_OPTION_ARG_NONE,     &parsed_options.show_info,     "Show information about a connected device.", NULL },
    { "watch",          'w', 0, G_OPTION_ARG_NONE,     &parsed_options.watch_devices, "Watch iDevice insertions/removals", NULL },
    { "domain",         'd', 0, G_OPTION_ARG_STRING,   &parsed_options.domain,        "Get information about the specified domain. Default: None", "DOMAIN" },
    { "key",            'k', 0, G_OPTION_ARG_STRING,   &parsed_options.key,           "Only query key specified by NAME. Default: All keys.", "KEY" },
    { "xml",            'x', 0, G_OPTION_ARG_NONE,     &parsed_options.xml,           "Output information as an XML plist instead of key/value pairs", NULL },
    { NULL,             0,   0, G_OPTION_ARG_NONE,     NULL,                           NULL, NULL }
};


static gboolean parse_cmdline(int *argc, char ***argv)
{
    GOptionContext *context;
    GOptionGroup *group;
    GError *error = NULL;
    gboolean parsed;

    context = g_option_context_new(NULL);
    g_option_context_set_summary(context, "Display information about connected iOS devices.");
    group = g_option_group_new(NULL, NULL, NULL, &parsed_options, NULL);
    g_option_group_add_entries(group, common_options);
    g_option_context_set_main_group(context, group);
    parsed = g_option_context_parse(context, argc, argv, &error);
    g_option_context_free(context);
    if (!parsed) {
        g_warning("failed to parse options: %s", error->message);
    }
    return parsed;
}


static void handle_list_devices(void)
{
    GimdDeviceMonitor *monitor;
    GList *devices;
    GList *it;

    monitor = gimd_device_monitor_get();
    devices = gimd_device_monitor_get_connected_devices(monitor);
    for (it = devices; it != NULL; it = it->next) {
        char *udid;

        g_warn_if_fail(GIMD_IS_DEVICE(it->data));
        g_object_get(G_OBJECT(it->data), "udid", &udid, NULL);
        g_print("%s\n", udid);
        g_free(udid);
    }
    g_list_free(devices);
}


static void device_connected_disconnected_cb(GimdDeviceMonitor *monitor,
                                             GimdDevice *device,
                                             gpointer user_data)
{
    char *udid;

    g_object_get(G_OBJECT(device), "udid", &udid, NULL);
    g_print("%s %s\n", udid, (char *)user_data);
    g_free(udid);
}


static void handle_watch_devices(void)
{
    GimdDeviceMonitor *monitor;
    GMainLoop *main_loop;

    monitor = gimd_device_monitor_get();

    g_signal_connect(G_OBJECT(monitor), "device-connected",
                     (GCallback)device_connected_disconnected_cb,
                     "(connected)");
    g_signal_connect(G_OBJECT(monitor), "device-disconnected",
                     (GCallback)device_connected_disconnected_cb,
                     "(disconnected)");

    main_loop = g_main_loop_new(NULL, FALSE);
    g_main_loop_run(main_loop);
}


static gboolean check_valid_domain(GimdDevice *device, const char *domain)
{
    GStrv domains = gimd_device_get_domains(device);

    if (domain == NULL) {
        return TRUE;
    }

    while (*domains != NULL) {
        if (g_strcmp0(*domains, domain) == 0) {
            return TRUE;
        }
        domains++;
    }

    return FALSE;
}


static void handle_show_device_info(const char *udid, const char *domain,
                                    const char *key, gboolean xml)
{
    plist_t info = NULL;
    GimdDevice *device;

    if (udid == NULL) {
        g_warning("Device UDID is mandatory");
        return;
    }

    device = gimd_device_new(udid);
    if (!check_valid_domain(device, domain)) {
        /* FIXME: Print error, valid domains */
        g_warning("Invalid domain: %s", domain);
        goto end;
    }

    info = gimd_device_lookup(device, domain, key);
    if (info == NULL) {
        g_warning("No info for (%s, %s)", domain, key);
        goto end;
    }

    if (!xml) {
        g_warning("non-xml display not implemented");
        //plist_print_to_stream(info, stdout);
        xml = TRUE;
    }

    if (xml) {
        char *xml_str = NULL;
        uint32_t xml_len;

        plist_to_xml(info, &xml_str, &xml_len);
        g_print("%s\n", xml_str);
        free(xml_str);
    }
end:
    if (device != NULL) {
        g_object_unref(G_OBJECT(device));
    }
    if (info != NULL) {
        plist_free(info);
    }
}


int main(int argc, char **argv)
{
    GimdDevice *device;

    if (!parse_cmdline(&argc, &argv)) {
        return 1;
    }

    device = gimd_device_new(parsed_options.udid);
    g_assert(device != NULL);

    if (parsed_options.list_devices) {
        handle_list_devices();
    } else if (parsed_options.watch_devices) {
        handle_watch_devices();
    } else if (parsed_options.show_info) {
        handle_show_device_info(parsed_options.udid, parsed_options.domain,
                                parsed_options.key, parsed_options.xml);
    }
    g_object_unref(device);

    return 0;
}
