/*
 * ideviceinstall2: Clone of ideviceinstall using libgimd
 *
 * Copyright (C) 2015 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Author: Christophe Fergeau <cfergeau@redhat.com>
 */
#include <glib.h>
#include <libgimd/gimd-application.h>
#include <libgimd/gimd-device.h>

typedef struct {
    gboolean set;
    char *value;
} IdeviceInstallOptionCommand;

typedef struct {
    char *udid;
    gboolean list_apps;
    gboolean list_archives;
    IdeviceInstallOptionCommand install;
    IdeviceInstallOptionCommand uninstall;
    IdeviceInstallOptionCommand upgrade;
    IdeviceInstallOptionCommand archive;
    IdeviceInstallOptionCommand restore;
    IdeviceInstallOptionCommand remove_archive;
    char *options;
    gboolean debug;
} IdeviceInstallCmdlineOptions;

IdeviceInstallCmdlineOptions parsed_options;

static gboolean parse_option_command(const gchar *option_name, const gchar *value,
                                     gpointer data, GError **error)
{
    IdeviceInstallCmdlineOptions *options = data;

    if ((g_strcmp0(option_name, "--install") == 0) ||
       (g_strcmp0(option_name, "-i") == 0)) {
        options->install.set = TRUE;
        options->install.value = g_strdup(value);
    } else if ((g_strcmp0(option_name, "uninstall") == 0) ||
               (g_strcmp0(option_name, "-U") == 0)) {
        options->uninstall.set = TRUE;
        options->uninstall.value = g_strdup(value);
    } else if ((g_strcmp0(option_name, "upgrade") == 0) ||
               (g_strcmp0(option_name, "-g") == 0)) {
        options->upgrade.set = TRUE;
        options->upgrade.value = g_strdup(value);
    } else if ((g_strcmp0(option_name, "archive") == 0) ||
               (g_strcmp0(option_name, "-a") == 0)) {
        options->archive.set = TRUE;
        options->archive.value = g_strdup(value);
    } else if ((g_strcmp0(option_name, "restore") == 0) ||
               (g_strcmp0(option_name, "-r") == 0)) {
        options->restore.set = TRUE;
        options->restore.value = g_strdup(value);
    } else if ((g_strcmp0(option_name, "remove-archive") == 0) ||
               (g_strcmp0(option_name, "-R") == 0)) {
        options->remove_archive.set = TRUE;
        options->remove_archive.value = g_strdup(value);
    } else {
        g_warn_if_reached();
        g_set_error(error, G_OPTION_ERROR, G_OPTION_ERROR_FAILED,
                    "Unknow option: %s", option_name);
        return FALSE;
    }
    return TRUE;
}

static GOptionEntry common_options[] = {
    { "udid",           'u', 0, G_OPTION_ARG_STRING,   &parsed_options.udid,          "Target specific device by its 40-digit device UDID.", "UDID" },
    { "list-apps",      'l', 0, G_OPTION_ARG_NONE,     &parsed_options.list_apps,     "List applications.", NULL },
    { "list-archives",  'L', 0, G_OPTION_ARG_NONE,     &parsed_options.list_archives, "List archived applications.", NULL },
    { "install",        'i', 0, G_OPTION_ARG_CALLBACK, parse_option_command,          "Install app from package file specified by ARCHIVE. ARCHIVE can also be a .ipcc file for carrier bundles.", "ARCHIVE" },
    { "uninstall",      'U', 0, G_OPTION_ARG_CALLBACK, parse_option_command,          "Uninstall app specified by APPID.", "APPID" },
    { "upgrade",        'g', 0, G_OPTION_ARG_CALLBACK, parse_option_command,          "Upgrade app from package file specified by ARCHIVE.", "ARCHIVE" },
    { "archive",        'a', 0, G_OPTION_ARG_CALLBACK, parse_option_command,          "Archive app specified by APPID.", "APPID" },
    { "restore",        'r', 0, G_OPTION_ARG_CALLBACK, parse_option_command,          "Restore archived app specified by APPID.", "APPID" },
    { "remove-archive", 'R', 0, G_OPTION_ARG_CALLBACK, parse_option_command,          "Remove app archive specified by APPID.", "APPID" },
    { "options",        'o', 0, G_OPTION_ARG_STRING,   &parsed_options.options,       "Pass additional options to the specified command.", NULL  },
    { "debug",          'd', 0, G_OPTION_ARG_NONE,     &parsed_options.debug,         "Enable communication debugging.", NULL },
    { NULL,             0,   0, G_OPTION_ARG_NONE,     NULL,                           NULL, NULL }
};


static gboolean parse_cmdline(int *argc, char ***argv)
{
    GOptionContext *context;
    GOptionGroup *group;
    GError *error = NULL;
    gboolean parsed;

    context = g_option_context_new(" - Manage apps on iOS devices.");
    group = g_option_group_new(NULL, NULL, NULL, &parsed_options, NULL);
    g_option_group_add_entries(group, common_options);
    g_option_context_set_main_group(context, group);
    parsed = g_option_context_parse(context, argc, argv, &error);
    g_option_context_free(context);
    if (!parsed) {
        g_warning("failed to parse options: %s", error->message);
    }
    return parsed;
}


static void print_gimd_application(GimdApplication *application)
{
    char *appid;
    char *appname;

    g_object_get(G_OBJECT(application),
                 "appid", &appid,
                 "display-name", &appname,
                 NULL);
    g_print("  %s - %s\n", appid, appname);
    g_free(appid);
    g_free(appname);
}


static void handle_list_apps(GimdDevice *device)
{
    GError *error = NULL;
    GList *apps;
    GList *it;

    apps = gimd_device_list_applications_sync(device, 0, &error);
    if (error != NULL) {
        g_warning("Failed to list applications: %s", error->message);
        return;
    }
    for (it = apps; it != NULL; it = it->next) {
        print_gimd_application(GIMD_APPLICATION(it->data));
    }
    g_list_free_full(apps, g_object_unref);
}


static void handle_list_archives(GimdDevice *device)
{
    GError *error = NULL;
    GList *apps;
    GList *it;

    apps = gimd_device_list_archived_applications_sync(device, 0, &error);
    if (error != NULL) {
        g_warning("Failed to list applications: %s", error->message);
        g_error_free(error);
        return;
    }
    for (it = apps; it != NULL; it = it->next) {
        print_gimd_application(GIMD_APPLICATION(it->data));
    }
    g_list_free_full(apps, g_object_unref);
}


static void handle_install(GimdDevice *device, const char *archive)
{
    GError *error = NULL;
    GFile *bundle = g_file_new_for_path(archive);

    gimd_device_install_application_sync(device, bundle, &error);
    if (error != NULL) {
        g_warning("Failed to install application bundle: %s", error->message);
        g_error_free(error);
    }
    g_object_unref(bundle);
}


static void handle_uninstall(GimdDevice *device, const char *appid)
{
    GimdApplication *app;
    GError *error = NULL;

    app = gimd_application_new(appid);
    g_object_set(app, "device", device, NULL);
    gimd_application_uninstall_sync(app, 0, &error);
    if (error != NULL) {
        g_warning("Failed to uninstall application: %s", error->message);
        g_error_free(error);
    }
    g_object_unref(app);
}


static void handle_upgrade(GimdDevice *device, const char *archive)
{
    GError *error = NULL;
    GFile *bundle = g_file_new_for_path(archive);

    gimd_device_upgrade_application_sync(device, bundle, &error);
    if (error != NULL) {
        g_warning("Failed to upgrade application bundle: %s", error->message);
        g_error_free(error);
    }
    g_object_unref(bundle);
}


static void handle_archive(GimdDevice *device, const char *appid)
{
    GimdApplication *app;
    GError *error = NULL;

    app = gimd_application_new(appid);
    g_object_set(app, "device", device, NULL);
    gimd_application_archive_sync(app, 0, &error);
    if (error != NULL) {
        g_warning("Failed to archive application: %s", error->message);
        g_error_free(error);
    }
    g_object_unref(app);
}


static void handle_restore(GimdDevice *device, const char *appid)
{
    GimdApplication *app;
    GError *error = NULL;

    app = gimd_application_new(appid);
    g_object_set(app, "device", device, NULL);
    gimd_application_restore_archive_sync(app, 0, &error);
    if (error != NULL) {
        g_warning("Failed to restore archive: %s", error->message);
        g_error_free(error);
    }
    g_object_unref(app);
}


static void handle_remove_archive(GimdDevice *device, const char *appid)
{
    GimdApplication *app;
    GError *error = NULL;

    app = gimd_application_new(appid);
    g_object_set(app, "device", device, NULL);
    gimd_application_remove_archive_sync(app, 0, &error);
    if (error != NULL) {
        g_warning("Failed to remove archive: %s", error->message);
        g_error_free(error);
    }
    g_object_unref(app);
}


int main(int argc, char **argv)
{
    GimdDevice *device;

    if (!parse_cmdline(&argc, &argv)) {
        return 1;
    }
    if (!parsed_options.udid) {
        g_warning("Device UDID is mandatory");
        return 1;
    }
    device = gimd_device_new(parsed_options.udid);
    g_assert(device != NULL);

    if (parsed_options.options) {
        g_print("options: %s\n", parsed_options.options);
    }
    if (parsed_options.debug) {
        g_print("debug\n");
    }

    if (parsed_options.list_apps) {
        handle_list_apps(device);
    } else if (parsed_options.list_archives) {
        handle_list_archives(device);
    } else if (parsed_options.install.set) {
        handle_install(device, parsed_options.install.value);
    } else if (parsed_options.uninstall.set) {
        handle_uninstall(device, parsed_options.uninstall.value);
    } else if (parsed_options.upgrade.set) {
        handle_upgrade(device, parsed_options.upgrade.value);
    } else if (parsed_options.archive.set) {
        handle_archive(device, parsed_options.archive.value);
    } else if (parsed_options.restore.set) {
        handle_restore(device, parsed_options.restore.value);
    } else if (parsed_options.remove_archive.set) {
        handle_remove_archive(device, parsed_options.remove_archive.value);
    }
    g_object_unref(device);

    return 0;
}
