/*
 * gimd-imobiledevice-utils.c: low-level interaction with libimobiledevice
 *
 * Copyright (C) 2010 Nikias Bassen <nikias@gmx.li>
 * Copyright (C) 2013-2014 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Author: Christophe Fergeau <cfergeau@redhat.com>
 */
#include <config.h>

#include <glib.h>

#include "gimd-imobiledevice-utils.h"

lockdownd_client_t gimd_utils_lockdownd_new(const char *udid)
{
    idevice_t phone = NULL;
    lockdownd_client_t client = NULL;
    lockdownd_error_t lerr;

    if (idevice_new(&phone, udid) != IDEVICE_E_SUCCESS) {
        g_debug("Could not find device %s", udid);
        goto end;
    }

    lerr = lockdownd_client_new_with_handshake(phone, &client, "libgimd");
    if (lerr != LOCKDOWN_E_SUCCESS) {
        g_debug("Could not connect to lockdownd");
        goto end;
    }

end:
    idevice_free(phone);

    return client;
}


instproxy_client_t gimd_utils_instproxy_new(const char *udid)
{
    idevice_t phone = NULL;
    lockdownd_client_t client = NULL;
    lockdownd_service_descriptor_t service = NULL;
    instproxy_client_t instproxy = NULL;
    instproxy_error_t ierr;
    lockdownd_error_t lerr;

    if (idevice_new(&phone, udid) != IDEVICE_E_SUCCESS) {
        g_debug("Could not find device %s", udid);
        goto end;
    }

    lerr = lockdownd_client_new_with_handshake(phone, &client, "libgimd");
    if (lerr != LOCKDOWN_E_SUCCESS) {
        g_debug("Could not connect to lockdownd");
        goto end;
    }
    lerr = lockdownd_start_service(client, INSTPROXY_SERVICE_NAME, &service);
    if (lerr != LOCKDOWN_E_SUCCESS || !service) {
        g_debug("Could not start com.apple.mobile.installation_proxy");
        goto end;
    }

    ierr = instproxy_client_new(phone, service, &instproxy);

    if (ierr != INSTPROXY_E_SUCCESS) {
        g_warn_if_fail(instproxy == NULL);
        g_debug("Could not connect to installation proxy");
        goto end;
    }

end:
    lockdownd_service_descriptor_free(service);
    lockdownd_client_free(client);
    idevice_free(phone);

    return instproxy;
}

