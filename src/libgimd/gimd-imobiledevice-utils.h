/*
 * gimd-imobiledevice-utils.h: low-level interaction with libimobiledevice
 *
 * Copyright (C) 2014 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Author: Christophe Fergeau <cfergeau@redhat.com>
 */
#ifndef __GIMD_IMOBILEDEVICE_UTILS_H__
#define __GIMD_IMOBILEDEVICE_UTILS_H__

#include <libimobiledevice/installation_proxy.h>

G_BEGIN_DECLS

lockdownd_client_t gimd_utils_lockdownd_new(const char *udid);
instproxy_client_t gimd_utils_instproxy_new(const char *udid);

G_END_DECLS

#endif /* __GIMD_IMOBILEDEVICE_UTILS_H__ */
