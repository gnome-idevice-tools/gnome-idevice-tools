/*
 * gimd-plist-utils.h: libplist helpers
 *
 * Copyright (C) 2014 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Author: Christophe Fergeau <cfergeau@redhat.com>
 */
#ifndef __GIMD_PLIST_UTILS_H__
#define __GIMD_PLIST_UTILS_H__

#include <glib.h>
#include <plist/plist.h>

G_BEGIN_DECLS

char *gimd_utils_get_plist_dict_string(plist_t dict, const char *key);
guint64 gimd_utils_get_plist_dict_uint64(plist_t dict, const char *key,
                                         gboolean *found);
void gimd_utils_dump_plist(plist_t plist);

G_END_DECLS

#endif /* __GIMD_PLIST_UTILS_H__ */
