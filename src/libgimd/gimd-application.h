/*
 * gimd-application.h: class representing an application
 *
 * Copyright (C) 2012-2014 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Author: Christophe Fergeau <cfergeau@redhat.com>
 */
#ifndef __GIMD_APPLICATION_H__
#define __GIMD_APPLICATION_H__

#include <glib-object.h>
#include <gio/gio.h>
#include <plist/plist.h>

G_BEGIN_DECLS

#define GIMD_TYPE_APPLICATION            (gimd_application_get_type ())
#define GIMD_APPLICATION(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GIMD_TYPE_APPLICATION, GimdApplication))
#define GIMD_APPLICATION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GIMD_TYPE_APPLICATION, GimdApplicationClass))
#define GIMD_IS_APPLICATION(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GIMD_TYPE_APPLICATION))
#define GIMD_IS_APPLICATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GIMD_TYPE_APPLICATION))
#define GIMD_APPLICATION_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GIMD_TYPE_APPLICATION, GimdApplicationClass))

typedef struct _GimdApplication GimdApplication;
typedef struct _GimdApplicationPrivate GimdApplicationPrivate;
typedef struct _GimdApplicationClass GimdApplicationClass;

struct _GimdApplication
{
    GObject parent;

    GimdApplicationPrivate *priv;

    /* Do not add fields to this struct */
};

struct _GimdApplicationClass
{
    GObjectClass parent_class;

    gpointer padding[20];
};

typedef enum {
    GIMD_APPLICATION_ARCHIVE_UNINSTALL = 1 << 0,
    GIMD_APPLICATION_ARCHIVE_APPLICATION_ONLY = 1 << 1,
    GIMD_APPLICATION_ARCHIVE_DOCUMENTS_ONLY = 2 << 1,
} GimdApplicationArchiveFlags;

typedef void (*GimdProgressCallback)(guint percent_complete,
                                     gpointer user_data);

GType gimd_application_get_type(void);

GimdApplication *gimd_application_new(const char *appid);

GimdApplication *gimd_application_new_from_plist(plist_t app_plist);

gboolean gimd_application_archive_sync(GimdApplication *application,
                                       guint flags,
                                       GError **error);
void gimd_application_archive(GimdApplication *application,
                              guint flags,
                              GCancellable *cancellable,
                              GimdProgressCallback progress_callback,
                              gpointer progress_callback_data,
                              GAsyncReadyCallback callback,
                              gpointer user_data);
gboolean gimd_application_archive_finish(GimdApplication *application,
                                         GAsyncResult *result,
                                         GError **error);

gboolean gimd_application_remove_archive_sync(GimdApplication *application,
                                              guint flags,
                                              GError **error);
void gimd_application_remove_archive(GimdApplication *archive,
                                     guint flags,
                                     GCancellable *cancellable,
                                     GimdProgressCallback progress_callback,
                                     gpointer progress_callback_data,
                                     GAsyncReadyCallback callback,
                                     gpointer user_data);
gboolean gimd_application_remove_archive_finish(GimdApplication *archive,
                                                GAsyncResult *result,
                                                GError **error);

gboolean gimd_application_uninstall_sync(GimdApplication *application,
                                         guint flags,
                                         GError **error);
void gimd_application_uninstall(GimdApplication *application,
                                guint flags,
                                GCancellable *cancellable,
                                GimdProgressCallback progress_callback,
                                gpointer progress_callback_data,
                                GAsyncReadyCallback callback,
                                gpointer user_data);
gboolean gimd_application_uninstall_finish(GimdApplication *archive,
                                           GAsyncResult *result,
                                           GError **error);

gboolean gimd_application_restore_archive_sync(GimdApplication *application,
                                               guint flags,
                                               GError **error);
void gimd_application_restore_archive(GimdApplication *archive,
                                      guint flags,
                                      GCancellable *cancellable,
                                      GimdProgressCallback progress_callback,
                                      gpointer progress_callback_data,
                                      GAsyncReadyCallback callback,
                                      gpointer user_data);
gboolean gimd_application_restore_archive_finish(GimdApplication *archive,
                                                 GAsyncResult *result,
                                                 GError **error);

GFile *gimd_archived_application_get_gfile(GimdApplication *application);

G_END_DECLS

#endif /* __GIMD_APPLICATION_H__ */
