/*
 * gimd-device-monitor.c: class monitoring insertion/removal of iDevices
 *
 * Copyright (C) 2015 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Author: Christophe Fergeau <cfergeau@redhat.com>
 */
#include <config.h>

#include "gimd-device-monitor.h"
#include "gimd-device.h"

#include <libimobiledevice/libimobiledevice.h>

#define GIMD_DEVICE_MONITOR_GET_PRIVATE(obj)                         \
        (G_TYPE_INSTANCE_GET_PRIVATE((obj), GIMD_TYPE_DEVICE_MONITOR, GimdDeviceMonitorPrivate))

struct _GimdDeviceMonitorPrivate {
    GHashTable *devices;
};

G_DEFINE_TYPE(GimdDeviceMonitor, gimd_device_monitor, G_TYPE_OBJECT);

enum {
    GIMD_DEVICE_MONITOR_SIGNAL_DEVICE_CONNECTED,
    GIMD_DEVICE_MONITOR_SIGNAL_DEVICE_DISCONNECTED,
    GIMD_DEVICE_MONITOR_SIGNAL_LAST,
};

static guint signals[GIMD_DEVICE_MONITOR_SIGNAL_LAST];

static void gimd_device_monitor_finalize(GObject *object)
{
    GimdDeviceMonitor *monitor = GIMD_DEVICE_MONITOR(object);

    g_hash_table_unref(monitor->priv->devices);

    idevice_event_unsubscribe();

    G_OBJECT_CLASS(gimd_device_monitor_parent_class)->finalize(object);
}


static void gimd_device_monitor_class_init(GimdDeviceMonitorClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS(klass);

    g_type_class_add_private(klass, sizeof(GimdDeviceMonitorPrivate));
    signals[GIMD_DEVICE_MONITOR_SIGNAL_DEVICE_CONNECTED] =
        g_signal_new("device-connected", GIMD_TYPE_DEVICE_MONITOR,
                     G_SIGNAL_RUN_FIRST, 0, NULL, NULL, NULL,
                     G_TYPE_NONE,
                     1, GIMD_TYPE_DEVICE);
    signals[GIMD_DEVICE_MONITOR_SIGNAL_DEVICE_DISCONNECTED] =
        g_signal_new("device-disconnected", GIMD_TYPE_DEVICE_MONITOR,
                     G_SIGNAL_RUN_FIRST, 0, NULL, NULL, NULL,
                     G_TYPE_NONE,
                     1, GIMD_TYPE_DEVICE);

    object_class->finalize = gimd_device_monitor_finalize;
}


static void gimd_device_monitor_device_added(GimdDeviceMonitor *monitor,
                                             const char *udid)
{
    GimdDevice *device;

    if (g_hash_table_lookup(monitor->priv->devices, udid)) {
        g_debug("%s already known to GimdDeviceMonitor, not emitting signal",
                udid);
        return;
    }

    device = gimd_device_new(udid);
    g_hash_table_insert(monitor->priv->devices, g_strdup(udid), device);
    g_signal_emit(monitor,
                  signals[GIMD_DEVICE_MONITOR_SIGNAL_DEVICE_CONNECTED],
                  0, device);
}


static void gimd_device_monitor_device_removed(GimdDeviceMonitor *monitor,
                                               const char *udid)
{
    GimdDevice *device;

    device = g_hash_table_lookup(monitor->priv->devices, udid);
    if (device == NULL) {
        g_debug("%s is unknown to GimdDeviceMonitor, not emitting signal",
                udid);
        return;
    }
    g_signal_emit(monitor,
                  signals[GIMD_DEVICE_MONITOR_SIGNAL_DEVICE_DISCONNECTED],
                  0, device);
    g_hash_table_remove(monitor->priv->devices, udid);
}


typedef struct {
    GimdDeviceMonitor *monitor;
    gboolean device_added;
    char *udid;
} GimdDeviceMonitorEventIdleClosure;


static gboolean gimd_device_monitor_handle_event_idle_cb(gpointer user_data)
{
    GimdDeviceMonitorEventIdleClosure *closure = user_data;

    if (closure->device_added) {
        gimd_device_monitor_device_added(closure->monitor, closure->udid);
    } else {
        gimd_device_monitor_device_removed(closure->monitor, closure->udid);
    }
    g_object_unref(closure->monitor);
    g_free(closure->udid);
    g_free(closure);

    return G_SOURCE_REMOVE;
}


static void idevice_event_cb(const idevice_event_t *event, void *user_data)
{
    GimdDeviceMonitor *monitor = GIMD_DEVICE_MONITOR(user_data);
    GimdDeviceMonitorEventIdleClosure *closure;

    g_return_if_fail(event->conn_type == 1);
    g_return_if_fail(event->udid != NULL);

    /* This is running in a libimobiledevice thread,
     * we need to do the event handling in the main loop thread
     */
    closure = g_new0(GimdDeviceMonitorEventIdleClosure, 1);
    closure->monitor = g_object_ref(monitor);
    closure->udid = g_strdup(event->udid);

    switch (event->event) {
        case IDEVICE_DEVICE_ADD:
            closure->device_added = TRUE;
            g_idle_add(gimd_device_monitor_handle_event_idle_cb, closure);
            break;

        case IDEVICE_DEVICE_REMOVE:
            closure->device_added = FALSE;
            g_idle_add(gimd_device_monitor_handle_event_idle_cb, closure);
            break;

        default:
            g_warn_if_reached();
    }
}


static void gimd_device_monitor_cold_plug(GimdDeviceMonitor *monitor)
{
    GStrv devices;
    int count;
    int i;
    idevice_error_t status;

    status = idevice_get_device_list(&devices, &count);
    if (status != IDEVICE_E_SUCCESS) {
        g_debug("Error fetching device list: %d", status);
        return;
    }

    for (i = 0; i < count; i++) {
        gimd_device_monitor_device_added(monitor, devices[i]);
    }
    idevice_device_list_free(devices);
}


static void gimd_device_monitor_init(GimdDeviceMonitor *monitor)
{
    monitor->priv = GIMD_DEVICE_MONITOR_GET_PRIVATE(monitor);

    monitor->priv->devices = g_hash_table_new_full(g_str_hash, g_str_equal,
                                                   g_free, g_object_unref);
    idevice_event_subscribe(idevice_event_cb, monitor);
    gimd_device_monitor_cold_plug(monitor);
}


static GimdDeviceMonitor *gimd_device_monitor_new(void)
{
    return GIMD_DEVICE_MONITOR(g_object_new(GIMD_TYPE_DEVICE_MONITOR, NULL));
}


/**
 * gimd_device_monitor_get:
 *
 * Returns a pointer to the #GimdDeviceMonitor singleton instance, creating it
 * if necessary.
 * Returns: (transfer none):
 */
GimdDeviceMonitor *gimd_device_monitor_get(void)
{
    static GOnce create_monitor_once = G_ONCE_INIT;

    g_once(&create_monitor_once, (GThreadFunc)gimd_device_monitor_new, NULL);

    return create_monitor_once.retval;
}


/**
 * gimd_device_monitor_get_connected_devices:
 * @monitor: a #GimdDeviceMonitor

 * Gets the list of iOS devices currently connected. The returned list should be
 * freed with g_list_free().
 *
 * Returns: (element-type Gimd.Device) (transfer container):
 * a newly allocated #GList of #GimdDevice.
 */
GList *gimd_device_monitor_get_connected_devices(GimdDeviceMonitor *monitor)
{
    return g_hash_table_get_values(monitor->priv->devices);
}
