/*
 * gimd-device-monitor.h: class monitoring insertion/removal of iDevices
 *
 * Copyright (C) 2015 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Author: Christophe Fergeau <cfergeau@redhat.com>
 */
#ifndef __GIMD_DEVICE_MONITOR_H__
#define __GIMD_DEVICE_MONITOR_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define GIMD_TYPE_DEVICE_MONITOR            (gimd_device_monitor_get_type ())
#define GIMD_DEVICE_MONITOR(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GIMD_TYPE_DEVICE_MONITOR, GimdDeviceMonitor))
#define GIMD_DEVICE_MONITOR_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GIMD_TYPE_DEVICE_MONITOR, GimdDeviceMonitorClass))
#define GIMD_IS_DEVICE_MONITOR(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GIMD_TYPE_DEVICE_MONITOR))
#define GIMD_IS_DEVICE_MONITOR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GIMD_TYPE_DEVICE_MONITOR))
#define GIMD_DEVICE_MONITOR_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GIMD_TYPE_DEVICE_MONITOR, GimdDeviceMonitorClass))

typedef struct _GimdDeviceMonitor GimdDeviceMonitor;
typedef struct _GimdDeviceMonitorPrivate GimdDeviceMonitorPrivate;
typedef struct _GimdDeviceMonitorClass GimdDeviceMonitorClass;

struct _GimdDeviceMonitor
{
    GObject parent;

    GimdDeviceMonitorPrivate *priv;

    /* Do not add fields to this struct */
};

struct _GimdDeviceMonitorClass
{
    GObjectClass parent_class;

    gpointer padding[20];
};

GType gimd_device_monitor_get_type(void);

GimdDeviceMonitor *gimd_device_monitor_get(void);

GList *gimd_device_monitor_get_connected_devices(GimdDeviceMonitor *monitor);

G_END_DECLS

#endif /* __GIMD_DEVICE_MONITOR_H__ */

