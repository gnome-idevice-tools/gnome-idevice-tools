/*
 * gimd-application.c: class representing an application
 *
 * Copyright (C) 2010 Nikias Bassen <nikias@gmx.li>
 * Copyright (C) 2013-2014 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Author: Christophe Fergeau <cfergeau@redhat.com>
 */

#include <config.h>

#include <libimobiledevice/installation_proxy.h>

#include <stdlib.h>

#include "gimd-application.h"
#include "gimd-application-priv.h"
#include "gimd-device.h"
#include "gimd-error.h"
#include "gimd-imobiledevice-utils.h"
#include "gimd-plist-utils.h"

#define GIMD_APPLICATION_GET_PRIVATE(obj)                         \
        (G_TYPE_INSTANCE_GET_PRIVATE((obj), GIMD_TYPE_APPLICATION, GimdApplicationPrivate))

struct _GimdApplicationPrivate {
    char *appid;
    char *display_name;
    char *version;
    GimdDevice *device;
};

G_DEFINE_TYPE(GimdApplication, gimd_application, G_TYPE_OBJECT);


enum {
    PROP_0,
    PROP_APPID,
    PROP_DEVICE,
    PROP_DISPLAY_NAME,
    PROP_VERSION,
};


typedef instproxy_error_t (*GimdInstproxyAsyncOp)(instproxy_client_t client,
                                                  const char *appid,
                                                  plist_t client_options,
                                                  instproxy_status_cb_t status_cb,
                                                  void *user_data);


static void gimd_application_get_property(GObject *object,
                                          guint prop_id,
                                          GValue *value,
                                          GParamSpec *pspec)
{
    GimdApplication *application = GIMD_APPLICATION(object);

    switch (prop_id) {
    case PROP_APPID:
        g_value_set_string(value, application->priv->appid);
        break;
    case PROP_DISPLAY_NAME:
        g_value_set_string(value, application->priv->display_name);
        break;
    case PROP_VERSION:
        g_value_set_string(value, application->priv->version);
        break;
    case PROP_DEVICE:
        g_value_set_object(value, application->priv->device);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
        break;
    }
}


static void gimd_application_set_property(GObject *object,
                                          guint prop_id,
                                          const GValue *value,
                                          GParamSpec *pspec)
{
    GimdApplication *application = GIMD_APPLICATION(object);

    switch (prop_id) {
    case PROP_APPID:
        g_free(application->priv->appid);
        application->priv->appid = g_value_dup_string(value);
        break;
    case PROP_DISPLAY_NAME:
        g_free(application->priv->display_name);
        application->priv->display_name = g_value_dup_string(value);
        break;
    case PROP_VERSION:
        g_free(application->priv->version);
        application->priv->version = g_value_dup_string(value);
        break;
    case PROP_DEVICE:
        if (application->priv->device != NULL) {
            g_object_unref(application->priv->device);
        }
        application->priv->device = g_value_dup_object(value);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
        break;
    }
}


static void gimd_application_dispose(GObject *object)
{
    GimdApplication *app = GIMD_APPLICATION(object);

    g_clear_object(&app->priv->device);

    G_OBJECT_CLASS(gimd_application_parent_class)->dispose(object);
}

static void gimd_application_finalize(GObject *object)
{
    GimdApplication *application = GIMD_APPLICATION(object);

    g_free(application->priv->appid);
    g_free(application->priv->display_name);
    g_free(application->priv->version);

    G_OBJECT_CLASS(gimd_application_parent_class)->finalize(object);
}


static void gimd_application_class_init(GimdApplicationClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS(klass);
    GParamSpec *param_spec;

    g_type_class_add_private(klass, sizeof(GimdApplicationPrivate));

    object_class->dispose = gimd_application_dispose;
    object_class->finalize = gimd_application_finalize;
    object_class->get_property = gimd_application_get_property;
    object_class->set_property = gimd_application_set_property;

    param_spec = g_param_spec_string("appid",
                                     "AppID",
                                     "Application ID",
                                     NULL,
                                     G_PARAM_READWRITE |
                                     G_PARAM_CONSTRUCT_ONLY |
                                     G_PARAM_STATIC_STRINGS);
    g_object_class_install_property(object_class,
                                    PROP_APPID,
                                    param_spec);

    param_spec = g_param_spec_string("display-name",
                                     "Display Name",
                                     "Application Display Name",
                                     NULL,
                                     G_PARAM_READWRITE |
                                     G_PARAM_STATIC_STRINGS);
    g_object_class_install_property(object_class,
                                    PROP_DISPLAY_NAME,
                                    param_spec);

    param_spec = g_param_spec_string("version",
                                     "Version",
                                     "Application Version",
                                     NULL,
                                     G_PARAM_READWRITE |
                                     G_PARAM_STATIC_STRINGS);
    g_object_class_install_property(object_class,
                                    PROP_VERSION,
                                    param_spec);

    param_spec = g_param_spec_object("device",
                                     "Device",
                                     "GimdDevice the application is associated with",
                                     GIMD_TYPE_DEVICE,
                                     G_PARAM_READWRITE |
                                     G_PARAM_STATIC_STRINGS);
    g_object_class_install_property(object_class,
                                    PROP_DEVICE,
                                    param_spec);
}


static void gimd_application_init(GimdApplication *application)
{
    application->priv = GIMD_APPLICATION_GET_PRIVATE(application);
}


GimdApplication *gimd_application_new(const char *appid)
{
    g_return_val_if_fail(appid != NULL, NULL);

    return GIMD_APPLICATION(g_object_new(GIMD_TYPE_APPLICATION, "appid", appid, NULL));
}


GimdApplication *gimd_application_new_from_plist(plist_t app_plist)
{
    char *appid;
    char *display_name;
    char *version;
    GimdApplication *app;

    g_return_val_if_fail(app_plist != NULL, NULL);

    appid = gimd_utils_get_plist_dict_string(app_plist, "CFBundleIdentifier");
    if (!appid) {
        g_warning("No appid in app plist");
        return NULL;
    }
    display_name = gimd_utils_get_plist_dict_string(app_plist,
                                                    "CFBundleDisplayName");
    version = gimd_utils_get_plist_dict_string(app_plist, "CFBundleVersion");

    app = gimd_application_new(appid);
    if (display_name != NULL) {
        g_object_set(G_OBJECT(app), "display-name", display_name, NULL);
    }
    if (version != NULL) {
        g_object_set(G_OBJECT(app), "version", version, NULL);
    }
    g_free(appid);
    g_free(display_name);
    g_free(version);

    return app;
}


static plist_t
gimd_application_options_from_flags(guint flags)
{
    plist_t client_opts;

    client_opts = instproxy_client_options_new();
    if ((flags & GIMD_APPLICATION_ARCHIVE_UNINSTALL) == 0) {
        /* Beware, iOS default is to uninstall the application */
        instproxy_client_options_add(client_opts,
                                     "SkipUninstall", 1,
                                     NULL);
    }

    if ((flags & GIMD_APPLICATION_ARCHIVE_APPLICATION_ONLY) != 0) {
        instproxy_client_options_add(client_opts,
                                     "ArchiveType", "ApplicationOnly",
                                     NULL);
    } else if ((flags & GIMD_APPLICATION_ARCHIVE_DOCUMENTS_ONLY) != 0) {
        instproxy_client_options_add(client_opts,
                                     "ArchiveType", "DocumentsOnly",
                                     NULL);
    }

    return client_opts;
}


typedef struct {
    gboolean done;
    GError *error;
    GMutex mutex;
    GCond cond;
} GimdOperationSyncData;


typedef struct {
    instproxy_client_t instproxy;
    GimdProgressCallback progress_cb;
    gpointer progress_cb_data;
} GimdOperationAsyncData;


static void gimd_operation_async_data_free(gpointer opaque)
{
    GimdOperationAsyncData *data = opaque;

    instproxy_client_free(data->instproxy);
    g_free(data);
}


typedef struct {
    GimdOperationAsyncData *data;
    guint percent_complete;
} GimdOperationProgressData;


static gboolean gimd_operation_async_progress_in_main(gpointer user_data)
{
    GimdOperationProgressData *progress_data = user_data;

    g_warn_if_fail(progress_data->data->progress_cb != NULL);

    progress_data->data->progress_cb(progress_data->percent_complete,
                                     progress_data->data->progress_cb_data);

    return G_SOURCE_REMOVE;
}


static void gimd_operation_report_progress(GTask *task,
                                           guint percent_complete)
{
    GimdOperationAsyncData *data = g_task_get_task_data(task);

    if (data->progress_cb != NULL) {
        GimdOperationProgressData *progress_data;

        progress_data = g_new0(GimdOperationProgressData, 1);
        progress_data->data = data;
        progress_data->percent_complete = percent_complete;
        g_main_context_invoke_full (g_task_get_context (task),
                                    g_task_get_priority (task),
                                    gimd_operation_async_progress_in_main,
                                    progress_data,
                                    g_free);
    }
}


static gboolean check_operation_status(plist_t command, plist_t status,
                                       guint64 *percent, GError **error)
{
    gboolean complete = FALSE;
    char *command_name;
    instproxy_command_get_name(command, &command_name);
    g_debug("Status for command '%s'",
            (command_name != NULL)?command_name:"<unknown>");
    free(command_name);

    if (status && command) {
        char *status_msg;
        char *error_msg = NULL;
        char *error_description = NULL;
        guint64 error_code;

        instproxy_status_get_name(status, &status_msg);
        g_debug("status: %s", status_msg);
        instproxy_status_get_error(status, &error_msg,
                                   &error_description,
                                   &error_code);

        if (g_strcmp0(status_msg, "Complete") == 0) {
            if (percent != NULL) {
                *percent = 100;
            }
            complete = TRUE;
        } else if (error_msg != NULL) {
            g_set_error(error, GIMD_ERROR, GIMD_ERROR_OPERATION_FAILED,
                        "Operation failed: %s",
                        error_description?error_description:error_msg);
            complete = FALSE;
        } else {
            if (percent != NULL) {
                int percent_int;
                instproxy_status_get_percent_complete(status, &percent_int);
                if (percent_int < 0) {
                    *percent = 0;
                } else {
                    *percent = percent_int;
                }
            }
            complete = FALSE;
        }
        free(status_msg);
        free(error_msg);
        free(error_description);

        return complete;
    } else {
        if (status != NULL) {
            char *xml;
            plist_to_xml(status, &xml, NULL);
            g_debug("Status: %s", xml);
            free(xml);
        } else {
            g_debug("Status: (none)");
        }
        g_warning("%s: called with invalid data!", G_STRFUNC);
        g_set_error_literal(error, GIMD_ERROR, GIMD_ERROR_OPERATION_FAILED,
                            "Operation failed: unexpected data");
        return FALSE;
    }

    g_warn_if_reached();

    return FALSE;
}


/* FIXME: need to watch device removal somehow */
static void async_operation_status_cb(plist_t operation,
                                      plist_t status,
                                      void *user_data)
{
    GTask *task = G_TASK(user_data);
    guint64 percent;
    GError *error = NULL;
    gboolean complete;

    complete = check_operation_status(operation, status, &percent, &error);
    if (complete) {
        g_task_return_boolean(task, TRUE);
        g_object_unref(task);
    } else if (error != NULL) {
        g_task_return_error(task, error);
        g_object_unref(task);
    } else {
        gimd_operation_report_progress(task, percent);
    }
}


static void gimd_application_async_operation(GimdApplication *application,
                                             GimdInstproxyAsyncOp async_op,
                                             plist_t options,
                                             GCancellable *cancellable,
                                             GimdProgressCallback progress_callback,
                                             gpointer progress_callback_data,
                                             GAsyncReadyCallback callback,
                                             gpointer user_data)
{
    instproxy_client_t instproxy = NULL;
    instproxy_error_t operation_status;
    GTask *task;
    GimdOperationAsyncData *task_data;
    char *udid;

    g_return_if_fail(GIMD_IS_APPLICATION(application));
    g_return_if_fail(application->priv->device != NULL);

    g_object_get(application->priv->device, "udid", &udid, NULL);
    instproxy = gimd_utils_instproxy_new(udid);
    g_free(udid);
    if (instproxy == NULL) {
        g_task_report_new_error(application, callback, user_data,
                                gimd_application_async_operation,
                                GIMD_ERROR,  GIMD_ERROR_FAILED,
                                "Could not connect to device");
        return;
    }

    task_data = g_new0(GimdOperationAsyncData, 1);
    task_data->progress_cb = progress_callback;
    task_data->progress_cb_data = progress_callback_data;
    task_data->instproxy = instproxy;

    task = g_task_new(application, cancellable, callback, user_data);
    g_task_set_task_data(task, task_data, gimd_operation_async_data_free);

    GTimer *timer = g_timer_new();
    operation_status = async_op(instproxy, application->priv->appid,
                       options, async_operation_status_cb, task);
    gdouble elapsed = g_timer_elapsed(timer, NULL);
    g_timer_destroy(timer);
    g_warning("libimobiledevice async call %p took %f seconds",
              async_op, elapsed);
    if (operation_status != INSTPROXY_E_SUCCESS) {
        if (operation_status == INSTPROXY_E_OP_IN_PROGRESS) {
            g_warning("An operation already in progress on the device");
        }
        g_task_return_new_error(task, GIMD_ERROR, GIMD_ERROR_OPERATION_FAILED,
                                "Asynchronous operation failed");
    }
}


/* FIXME: need to watch device removal somehow */
static void sync_operation_status_cb(plist_t operation,
                                     plist_t status,
                                     void *user_data)
{
    GimdOperationSyncData *data = user_data;
    gboolean complete;

    complete = check_operation_status(operation, status, NULL, &data->error);
    if (complete || data->error != NULL) {
        data->done = TRUE;
        g_mutex_lock(&data->mutex);
        g_cond_signal(&data->cond);
        g_mutex_unlock(&data->mutex);
    } else {
        /* If we reached here, the callback was called to give us progress
         * information on the current task, can be safely ignored */
    }
}


static gboolean gimd_application_sync_operation(GimdApplication *application,
                                                GimdInstproxyAsyncOp async_op,
                                                plist_t options,
                                                GError **error)
{
    instproxy_client_t instproxy = NULL;
    instproxy_error_t operation_status;
    char *udid;
    GimdOperationSyncData *data;
    gboolean success;

    g_return_if_fail(GIMD_IS_APPLICATION(application));
    g_return_if_fail(application->priv->device != NULL);

    g_object_get(application->priv->device, "udid", &udid, NULL);
    instproxy = gimd_utils_instproxy_new(udid);
    g_free(udid);
    if (instproxy == NULL) {
        g_set_error_literal(error, GIMD_ERROR, GIMD_ERROR_FAILED,
                            "Could not connect to device");
        return FALSE;
    }

    data = g_new0(GimdOperationSyncData, 1);
    g_mutex_init(&data->mutex);
    g_cond_init(&data->cond);
    g_mutex_lock(&data->mutex);
    operation_status = async_op(instproxy, application->priv->appid,
                                options, sync_operation_status_cb, data);
    if (operation_status != INSTPROXY_E_SUCCESS) {
        g_mutex_unlock(&data->mutex);
        g_set_error(error, GIMD_ERROR, GIMD_ERROR_OPERATION_FAILED,
                    "Synchronous operation failed");
        success = FALSE;
        goto end;
    }

    while (!data->done) {
        g_cond_wait(&data->cond, &data->mutex);
    }
    g_mutex_unlock(&data->mutex);

    if (data->error != NULL) {
        g_propagate_error(error, data->error);
        success = FALSE;
    } else {
        success = TRUE;
    }

end:
    g_cond_clear(&data->cond);
    g_mutex_clear(&data->mutex);
    g_free(data);
    instproxy_client_free(instproxy);

    return success;
}


/**
 * gimd_application_archive:
 * @application: a #GimdApplication
 * @flags: bitwise-OR of GimdApplicationArchiveFlags
 * @cancellable: optional #GCancellable object, %NULL to ignore
 * @progress_callback: function to callback with progress information, or %NULL
 * if progress information is not needed
 * @progress_callback_data: user data to pass to @progress_callback
 * @callback: a #GAsyncReadyCallback to call when the request is satisfied
 * @user_data: the data to pass to callback function
 *
 * Archive @application on the device it's stored on. GimdApplication:device
 * must be set. The application bundle will be stored on the device in the
 * ApplicationArchives folder. The application will then be uninstalled if
 * #GIMD_APPLICATION_ARCHIVE_UNINSTALL is set, otherwise it will be listed both in
 * gimd_device_list_applications() and gimd_device_list_archived_applications().
 * This operation is asynchronous.
 */
void gimd_application_archive(GimdApplication *application,
                              guint flags,
                              GCancellable *cancellable,
                              GimdProgressCallback progress_callback,
                              gpointer progress_callback_data,
                              GAsyncReadyCallback callback,
                              gpointer user_data)
{
    plist_t options;

    g_return_if_fail(GIMD_IS_APPLICATION(application));

    options = gimd_application_options_from_flags(flags);
    if (options == NULL) {
        g_task_report_new_error(application, callback, user_data,
                                gimd_application_archive,
                                GIMD_ERROR, GIMD_ERROR_INVALID_ARGUMENT,
                                "Invalid archive flags: 0x%x", flags);
        return;
    }

    gimd_application_async_operation(application, instproxy_archive,
                                     options, cancellable,
                                     progress_callback, progress_callback_data,
                                     callback, user_data);
    instproxy_client_options_free(options);
}


/**
 * gimd_application_archive_finish:
 * @application: a #GimdApplication
 * @result: a #GAsyncResult
 * @error: a #GError or %NULL
 *
 * Returns: TRUE if the operation was successful, FALSE otherwise.
 */
gboolean gimd_application_archive_finish(GimdApplication *application,
                                         GAsyncResult *result,
                                         GError **error)
{
    g_return_val_if_fail(GIMD_IS_APPLICATION(application), FALSE);
    g_return_val_if_fail((error == NULL) || (*error == NULL), FALSE);

    return g_task_propagate_boolean(G_TASK(result), error);
}


/**
 * gimd_application_archive_sync:
 * @application: a #GimdApplication
 * @flags: bitwise-OR of GimdApplicationArchiveFlags
 * @error: return location for a #GError or %NULL
 *
 * Archive @application on the device it's stored on. GimdApplication:device
 * must be set. The application bundle will be stored on the device in the
 * ApplicationArchives folder. The application will then be uninstalled if
 * #GIMD_APPLICATION_ARCHIVE_UNINSTALL is set. This operation is synchronous.
 */
gboolean gimd_application_archive_sync(GimdApplication *application,
                                       guint flags,
                                       GError **error)
{
    plist_t options;
    gboolean success;

    g_return_val_if_fail(GIMD_IS_APPLICATION(application), FALSE);
    g_return_val_if_fail((error == NULL) || (*error == NULL), FALSE);

    options = gimd_application_options_from_flags(flags);
    if (options == NULL) {
        g_set_error(error, GIMD_ERROR, GIMD_ERROR_INVALID_ARGUMENT,
                    "Invalid archive flags: 0x%x", flags);
        return FALSE;
    }

    success = gimd_application_sync_operation(application,
                                              instproxy_archive,
                                              options, error);
    instproxy_client_options_free(options);

    return success;
}


/**
 * gimd_application_remove_archive:
 * @application: a #GimdApplication
 * @flags: currently unused, must be 0.
 *
 * Remove the archive @archive on the device it's stored on.
 * GimdApplication:device must be set. This operation is asynchronous.
 */
void gimd_application_remove_archive(GimdApplication *archive,
                                     guint flags,
                                     GCancellable *cancellable,
                                     GimdProgressCallback progress_callback,
                                     gpointer progress_callback_data,
                                     GAsyncReadyCallback callback,
                                     gpointer user_data)
{
    g_return_if_fail(GIMD_IS_APPLICATION(archive));
    g_return_if_fail(flags == 0);

    gimd_application_async_operation(archive, instproxy_remove_archive,
                                     NULL, cancellable,
                                     progress_callback, progress_callback_data,
                                     callback, user_data);
}


/**
 * gimd_application_remove_archive_finish:
 * @archive: a #GimdApplication
 * @result: a #GAsyncResult
 * @error: a #GError or NULL
 *
 * Returns: TRUE if the operation was successful, FALSE otherwise.
 */
gboolean gimd_application_remove_archive_finish(GimdApplication *archive,
                                                GAsyncResult *result,
                                                GError **error)
{
    g_return_val_if_fail(GIMD_IS_APPLICATION(archive), FALSE);
    g_return_val_if_fail((error == NULL) || (*error == NULL), FALSE);

    return g_task_propagate_boolean(G_TASK(result), error);
}


/**
 * gimd_application_remove_archive_sync:
 * @application: a #GimdApplication
 * @flags: currently unused, must be 0.
 * @error: return location for a #GError or %NULL
 *
 * Remove the archive @archive on the device it's stored on.
 * GimdApplication:device must be set. This operation is synchronous.
 */
gboolean gimd_application_remove_archive_sync(GimdApplication *application,
                                              guint flags,
                                              GError **error)
{
    g_return_val_if_fail(GIMD_IS_APPLICATION(application), FALSE);
    g_return_val_if_fail(flags == 0, FALSE);
    g_return_val_if_fail((error == NULL) || (*error == NULL), FALSE);

    return gimd_application_sync_operation(application,
                                           instproxy_remove_archive,
                                           NULL, error);
}


/**
 * gimd_application_uninstall:
 * @application: a #GimdApplication
 * @flags: currently unused, must be 0.
 * @cancellable: optional #GCancellable object, %NULL to ignore
 * @progress_callback: function to callback with progress information, or %NULL
 * if progress information is not needed
 * @progress_callback_data: user data to pass to @progress_callback
 * @callback: a #GAsyncReadyCallback to call when the request is satisfied
 * @user_data: the data to pass to callback function
 *
 * Uninstall @application from the device it's stored on. GimdApplication:device
 * must be set. This operation is asynchronous.
 */
void gimd_application_uninstall(GimdApplication *application,
                                guint flags,
                                GCancellable *cancellable,
                                GimdProgressCallback progress_callback,
                                gpointer progress_callback_data,
                                GAsyncReadyCallback callback,
                                gpointer user_data)
{
    g_return_if_fail(GIMD_IS_APPLICATION(application));
    g_return_if_fail(flags == 0);

    gimd_application_async_operation(application, instproxy_uninstall,
                                     NULL, cancellable,
                                     progress_callback, progress_callback_data,
                                     callback, user_data);
}


/**
 * gimd_application_uninstall_finish:
 * @application: a #GimdApplication
 * @result: a #GAsyncResult
 * @error: a #GError or %NULL
 *
 * Returns: TRUE if the operation was successful, FALSE otherwise.
 */
gboolean gimd_application_uninstall_finish(GimdApplication *application,
                                           GAsyncResult *result,
                                           GError **error)
{
    g_return_val_if_fail(GIMD_IS_APPLICATION(application), FALSE);
    g_return_val_if_fail((error == NULL) || (*error == NULL), FALSE);

    return g_task_propagate_boolean(G_TASK(result), error);
}


/**
 * gimd_application_uninstall_sync:
 * @application: a #GimdApplication
 * @flags: currently unused, must be 0.
 * @error: return location for a #GError or %NULL
 *
 * Uninstall @application from the device it's stored on. GimdApplication:device
 * must be set. This operation is synchronous.
 */
gboolean gimd_application_uninstall_sync(GimdApplication *application,
                                         guint flags,
                                         GError **error)
{
    g_return_val_if_fail(GIMD_IS_APPLICATION(application), FALSE);
    g_return_val_if_fail(flags == 0, FALSE);
    g_return_val_if_fail((error == NULL) || (*error == NULL), FALSE);

    return gimd_application_sync_operation(application,
                                           instproxy_uninstall,
                                           NULL, error);
}


/**
 * gimd_application_restore_archive:
 * @archive: a #GimdApplication
 * @flags: currently unused, must be 0.
 * @cancellable: optional #GCancellable object, %NULL to ignore
 * @progress_callback: function to callback with progress information, or %NULL
 * if progress information is not needed
 * @progress_callback_data: user data to pass to @progress_callback
 * @callback: a #GAsyncReadyCallback to call when the request is satisfied
 * @user_data: the data to pass to callback function
 *
 * Restore @archive on the device it's stored on. GimdApplication:device
 * must be set. This operation is asynchronous.
 */
void gimd_application_restore_archive(GimdApplication *archive,
                                      guint flags,
                                      GCancellable *cancellable,
                                      GimdProgressCallback progress_callback,
                                      gpointer progress_callback_data,
                                      GAsyncReadyCallback callback,
                                      gpointer user_data)
{
    g_return_if_fail(GIMD_IS_APPLICATION(archive));
    g_return_if_fail(flags == 0);

    gimd_application_async_operation(archive, instproxy_restore,
                                     NULL, cancellable,
                                     progress_callback, progress_callback_data,
                                     callback, user_data);
}


/**
 * gimd_application_restore_archive_finish:
 * @archive: a #GimdApplication
 * @result: a #GAsyncResult
 * @error: a #GError or %NULL
 *
 * Returns: TRUE if the operation was successful, FALSE otherwise.
 */
gboolean gimd_application_restore_archive_finish(GimdApplication *archive,
                                                 GAsyncResult *result,
                                                 GError **error)
{
    g_return_val_if_fail(GIMD_IS_APPLICATION(archive), FALSE);
    g_return_val_if_fail((error == NULL) || (*error == NULL), FALSE);

    return g_task_propagate_boolean(G_TASK(result), error);
}


/**
 * gimd_application_restore_archive_sync:
 * @application: a #GimdApplication
 * @flags: currently unused, must be 0.
 * @error: return location for a #GError or %NULL
 *
 * Restore @archive on the device it's stored on. GimdApplication:device
 * must be set. This operation is synchronous.
 */
gboolean gimd_application_restore_archive_sync(GimdApplication *application,
                                               guint flags,
                                               GError **error)
{
    g_return_val_if_fail(GIMD_IS_APPLICATION(application), FALSE);
    g_return_val_if_fail(flags == 0, FALSE);
    g_return_val_if_fail((error == NULL) || (*error == NULL), FALSE);

    return gimd_application_sync_operation(application,
                                           instproxy_restore,
                                           NULL, error);
}


/**
 * gimd_application_install:
 * @application: a #GimdApplication
 * @client_opts: a plist describing the installation operation
 * @cancellable: optional #GCancellable object, %NULL to ignore
 * @progress_callback: function to callback with progress information, or %NULL
 * if progress information is not needed
 * @progress_callback_data: user data to pass to @progress_callback
 * @callback: a #GAsyncReadyCallback to call when the request is satisfied
 * @user_data: the data to pass to callback function
 *
 * Triggers installation of @application on the device. The application
 * bundle must have been copied to the right place first, and @client_opts
 * must contain details this.  GimdApplication:device must be set. This
 * operation is synchronous. This is an internal-only API.
 */
G_GNUC_INTERNAL void gimd_application_install(GimdApplication *application,
                                              plist_t client_opts,
                                              GCancellable *cancellable,
                                              GimdProgressCallback progress_callback,
                                              gpointer progress_callback_data,
                                              GAsyncReadyCallback callback,
                                              gpointer user_data)
{
    g_return_if_fail(GIMD_IS_APPLICATION(application));
    g_return_if_fail(client_opts != NULL);

    gimd_application_async_operation(application, instproxy_install,
                                     client_opts, cancellable,
                                     progress_callback, progress_callback_data,
                                     callback, user_data);
}


/**
 * gimd_application_install_finish:
 * @application: a #GimdApplication
 * @result: a #GAsyncResult
 * @error: a #GError or %NULL
 *
 * Returns: TRUE if the operation was successful, FALSE otherwise.
 */
G_GNUC_INTERNAL gboolean gimd_application_install_finish(GimdApplication *application,
                                                         GAsyncResult *result,
                                                         GError **error)
{
    g_return_val_if_fail(GIMD_IS_APPLICATION(application), FALSE);
    g_return_val_if_fail((error == NULL) || (*error == NULL), FALSE);

    return g_task_propagate_boolean(G_TASK(result), error);
}


/**
 * gimd_application_install_sync:
 * @application: a #GimdApplication
 * @client_opts: a plist describing the installation operation
 * @error: return location for a #GError or %NULL
 *
 * Triggers installation of @application on the device. The application
 * bundle must have been copied to the right place first, and @client_opts
 * must contain details this.  GimdApplication:device must be set. This
 * operation is synchronous. This is an internal-only API.
 */
G_GNUC_INTERNAL gboolean
gimd_application_install_sync(GimdApplication *application,
                              plist_t client_opts,
                              GError **error)
{
    g_return_val_if_fail(GIMD_IS_APPLICATION(application), FALSE);
    g_return_val_if_fail(client_opts != NULL, FALSE);
    g_return_val_if_fail((error == NULL) || (*error == NULL), FALSE);

    return gimd_application_sync_operation(application,
                                           instproxy_install,
                                           NULL, error);
}


/**
 * gimd_application_upgrade:
 * @application: a #GimdApplication
 * @client_opts: a plist describing the installation operation
 * @cancellable: optional #GCancellable object, %NULL to ignore
 * @progress_callback: function to callback with progress information, or %NULL
 * if progress information is not needed
 * @progress_callback_data: user data to pass to @progress_callback
 * @callback: a #GAsyncReadyCallback to call when the request is satisfied
 * @user_data: the data to pass to callback function
 *
 * Triggers upgrade of @application on the device. The application
 * bundle must have been copied to the right place first, and @client_opts
 * must contain details this.  GimdApplication:device must be set. This
 * operation is synchronous. This is an internal-only API.
 */
G_GNUC_INTERNAL void gimd_application_upgrade(GimdApplication *application,
                                              plist_t client_opts,
                                              GCancellable *cancellable,
                                              GimdProgressCallback progress_callback,
                                              gpointer progress_callback_data,
                                              GAsyncReadyCallback callback,
                                              gpointer user_data)
{
    g_return_if_fail(GIMD_IS_APPLICATION(application));
    g_return_if_fail(client_opts != NULL);

    gimd_application_async_operation(application, instproxy_upgrade,
                                     client_opts, cancellable,
                                     progress_callback, progress_callback_data,
                                     callback, user_data);
}


/**
 * gimd_application_upgrade_finish:
 * @application: a #GimdApplication
 * @result: a #GAsyncResult
 * @error: a #GError or %NULL
 *
 * Returns: TRUE if the operation was successful, FALSE otherwise.
 */
G_GNUC_INTERNAL gboolean gimd_application_upgrade_finish(GimdApplication *application,
                                                         GAsyncResult *result,
                                                         GError **error)
{
    g_return_val_if_fail(GIMD_IS_APPLICATION(application), FALSE);
    g_return_val_if_fail((error == NULL) || (*error == NULL), FALSE);

    return g_task_propagate_boolean(G_TASK(result), error);
}


/**
 * gimd_application_upgrade_sync:
 * @application: a #GimdApplication
 * @client_opts: a plist describing the upgrade operation
 * @error: return location for a #GError or %NULL
 *
 * Triggers upgrade of @application on the device. The application
 * bundle must have been copied to the right place first, and @client_opts
 * must contain details this.  GimdApplication:device must be set. This
 * operation is synchronous. This is an internal-only API.
 */
G_GNUC_INTERNAL gboolean
gimd_application_upgrade_sync(GimdApplication *application,
                              plist_t client_opts,
                              GError **error)
{
    g_return_val_if_fail(GIMD_IS_APPLICATION(application), FALSE);
    g_return_val_if_fail(client_opts != NULL, FALSE);
    g_return_val_if_fail((error == NULL) || (*error == NULL), FALSE);

    return gimd_application_sync_operation(application,
                                           instproxy_upgrade,
                                           NULL, error);
}


/* FIXME: add derived GimdArchivedApplication class ? */
GFile *gimd_archived_application_get_gfile(GimdApplication *application)
{
    char *rel_path;
    char *basename;
    GFile *gfile;

    g_return_val_if_fail(GIMD_IS_APPLICATION(application), NULL);
    g_return_val_if_fail(GIMD_IS_DEVICE(application->priv->device), NULL);
    g_return_val_if_fail(application->priv->appid != NULL, NULL);


    basename = g_strconcat(application->priv->appid, ".zip", NULL);
    rel_path = g_build_filename("ApplicationArchives", basename, NULL);
    g_free(basename);

    gfile = gimd_device_get_gfile(application->priv->device, rel_path);
    g_free(rel_path);

    return gfile;
}
