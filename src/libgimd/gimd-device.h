/*
 * gimd-device.h: class representing an iDevice
 *
 * Copyright (C) 2012-2014 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Author: Christophe Fergeau <cfergeau@redhat.com>
 */
#ifndef __GIMD_DEVICE_H__
#define __GIMD_DEVICE_H__

#include <glib-object.h>
#include <gio/gio.h>

#include "gimd-application.h"

G_BEGIN_DECLS

#define GIMD_TYPE_DEVICE            (gimd_device_get_type ())
#define GIMD_DEVICE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GIMD_TYPE_DEVICE, GimdDevice))
#define GIMD_DEVICE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GIMD_TYPE_DEVICE, GimdDeviceClass))
#define GIMD_IS_DEVICE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GIMD_TYPE_DEVICE))
#define GIMD_IS_DEVICE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GIMD_TYPE_DEVICE))
#define GIMD_DEVICE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GIMD_TYPE_DEVICE, GimdDeviceClass))

typedef struct _GimdDevice GimdDevice;
typedef struct _GimdDevicePrivate GimdDevicePrivate;
typedef struct _GimdDeviceClass GimdDeviceClass;

struct _GimdDevice
{
    GObject parent;

    GimdDevicePrivate *priv;

    /* Do not add fields to this struct */
};

struct _GimdDeviceClass
{
    GObjectClass parent_class;

    gpointer padding[20];
};

typedef enum {
    GIMD_APPLICATION_LIST_USER = 0 << 1,
    GIMD_APPLICATION_LIST_SYSTEM = 1 << 1,
    GIMD_APPLICATION_LIST_ALL = (GIMD_APPLICATION_LIST_SYSTEM
                                 | GIMD_APPLICATION_LIST_USER),
} GimdApplicationListFlags;


GType gimd_device_get_type(void);

GimdDevice *gimd_device_new(const char *udid);

GList *gimd_device_list_applications_sync(GimdDevice *device,
                                          guint flags,
                                          GError **error);
void gimd_device_list_applications(GimdDevice *device,
                                   guint flags,
                                   GCancellable *cancellable,
                                   GAsyncReadyCallback callback,
                                   gpointer user_data);
GList *gimd_device_list_applications_finish(GimdDevice *device,
                                            GAsyncResult *result,
                                            GError **error);

GList *gimd_device_list_archived_applications_sync(GimdDevice *device,
                                                   guint flags,
                                                   GError **error);
void gimd_device_list_archived_applications(GimdDevice *device,
                                            guint flags,
                                            GCancellable *cancellable,
                                            GAsyncReadyCallback callback,
                                            gpointer user_data);
GList *gimd_device_list_archived_applications_finish(GimdDevice *device,
                                                     GAsyncResult *result,
                                                     GError **error);

void gimd_device_install_application(GimdDevice *device,
                                     GFile *bundle,
                                     GCancellable *cancellable,
                                     GimdProgressCallback progress_callback,
                                     gpointer progress_callback_data,
                                     GAsyncReadyCallback callback,
                                     gpointer user_data);
gboolean gimd_device_install_application_finish(GimdDevice *device,
                                                GAsyncResult *result,
                                                GError **error);
gboolean gimd_device_install_application_sync(GimdDevice *device,
                                              GFile *bundle,
                                              GError **error);

void gimd_device_upgrade_application(GimdDevice *device,
                                     GFile *bundle,
                                     GCancellable *cancellable,
                                     GimdProgressCallback progress_callback,
                                     gpointer progress_callback_data,
                                     GAsyncReadyCallback callback,
                                     gpointer user_data);
gboolean gimd_device_upgrade_application_finish(GimdDevice *device,
                                                GAsyncResult *result,
                                                GError **error);
gboolean gimd_device_upgrade_application_sync(GimdDevice *device,
                                              GFile *bundle,
                                              GError **error);

GVolume *gimd_device_get_gvolume(GimdDevice *device);
GFile *gimd_device_get_gfile(GimdDevice *device, const char *rel_path);

const GStrv gimd_device_get_domains(GimdDevice *device);
plist_t gimd_device_lookup(GimdDevice *device, const char *domain, const char *key);

G_END_DECLS

#endif /* __GIMD_DEVICE_H__ */
