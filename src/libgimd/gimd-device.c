/*
 * gimd-device.c: class representing an iDevice
 *
 * Copyright (C) 2010 Nikias Bassen <nikias@gmx.li>
 * Copyright (C) 2013-2014 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Author: Christophe Fergeau <cfergeau@redhat.com>
 */

#include <config.h>

#include <stdlib.h>
#include <string.h>
#include <archive.h>
#include <archive_entry.h>
#include <libimobiledevice/installation_proxy.h>

#include "gimd-application.h"
#include "gimd-application-priv.h"
#include "gimd-device.h"
#include "gimd-error.h"
#include "gimd-imobiledevice-utils.h"
#include "gimd-plist-utils.h"

#define GIMD_DEVICE_GET_PRIVATE(obj)                         \
        (G_TYPE_INSTANCE_GET_PRIVATE((obj), GIMD_TYPE_DEVICE, GimdDevicePrivate))

struct _GimdDevicePrivate {
    char *udid;
};

G_DEFINE_TYPE(GimdDevice, gimd_device, G_TYPE_OBJECT);


enum {
    PROP_0,
    PROP_UDID
};


static void gimd_device_get_property(GObject *object,
                                     guint prop_id,
                                     GValue *value,
                                     GParamSpec *pspec)
{
    GimdDevice *device = GIMD_DEVICE(object);

    switch (prop_id) {
    case PROP_UDID:
        g_value_set_string(value, device->priv->udid);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
        break;
    }
}


static void gimd_device_set_property(GObject *object,
                                     guint prop_id,
                                     const GValue *value,
                                     GParamSpec *pspec)
{
    GimdDevice *device = GIMD_DEVICE(object);

    switch (prop_id) {
    case PROP_UDID:
        g_free(device->priv->udid);
        device->priv->udid = g_value_dup_string(value);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
        break;
    }
}


static void gimd_device_finalize(GObject *object)
{
    GimdDevice *device = GIMD_DEVICE(object);

    g_free(device->priv->udid);

    G_OBJECT_CLASS(gimd_device_parent_class)->finalize(object);
}


static void gimd_device_class_init(GimdDeviceClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS(klass);
    GParamSpec *param_spec;

    g_type_class_add_private(klass, sizeof(GimdDevicePrivate));

    object_class->finalize = gimd_device_finalize;
    object_class->get_property = gimd_device_get_property;
    object_class->set_property = gimd_device_set_property;

    param_spec = g_param_spec_string("udid",
                                     "UDID",
                                     "Unique Device ID",
                                     NULL,
                                     G_PARAM_READWRITE |
                                     G_PARAM_CONSTRUCT_ONLY |
                                     G_PARAM_STATIC_STRINGS);
    g_object_class_install_property(object_class,
                                    PROP_UDID,
                                    param_spec);
}


static void gimd_device_init(GimdDevice *device)
{
    device->priv = GIMD_DEVICE_GET_PRIVATE(device);
}


GimdDevice *gimd_device_new(const char *udid)
{
    return GIMD_DEVICE(g_object_new(GIMD_TYPE_DEVICE, "udid", udid, NULL));
}


static void
free_gobject_list(gpointer list)
{
    g_list_free_full(list, g_object_unref);
}


static plist_t
gimd_device_list_application_options_from_flags(guint flags)
{
    plist_t client_opts;

    if (flags == 0) {
        flags = GIMD_APPLICATION_LIST_USER;
    }

    if ((flags & GIMD_APPLICATION_LIST_ALL) == GIMD_APPLICATION_LIST_ALL) {
        return NULL;
    }

    client_opts = instproxy_client_options_new();
    if ((flags & GIMD_APPLICATION_LIST_USER) != 0) {
        instproxy_client_options_add(client_opts,
                                     "ApplicationType", "User",
                                     NULL);
    } else if ((flags & GIMD_APPLICATION_LIST_SYSTEM) != 0) {
        instproxy_client_options_add(client_opts,
                                     "ApplicationType", "System",
                                     NULL);
    }

    return client_opts;
}

/**
 * gimd_device_list_applications_sync:
 *
 * @device: an #GimdDevice
 * @flags: bitwise-or of GimdApplicationListFlags. If 0, only user applications
 * will be listed
 *
 * Gets the list of applications stored on @device.
 *
 * Returns: (element-type GimdApplication) (transfer full): a newly allocated list
 * of #GimdApplication
 **/
GList *gimd_device_list_applications_sync(GimdDevice *device,
                                          guint flags,
                                          GError **error)
{
    GList *apps = NULL;
    plist_t options;
    plist_t apps_plist = NULL;
    instproxy_client_t instproxy = NULL;
    instproxy_error_t err;
    unsigned int i;

    g_return_val_if_fail(GIMD_IS_DEVICE(device), NULL);
    g_return_val_if_fail((flags & ~GIMD_APPLICATION_LIST_ALL) == 0, NULL);

    instproxy = gimd_utils_instproxy_new(device->priv->udid);
    if (instproxy == NULL) {
        g_set_error(error, GIMD_ERROR, GIMD_ERROR_OPERATION_FAILED,
                    "Could not get applications list from device");
        goto end;
    }
    options = gimd_device_list_application_options_from_flags(flags);

    err = instproxy_browse(instproxy, options, &apps_plist);
    instproxy_client_options_free(options);
    if (err != INSTPROXY_E_SUCCESS) {
        g_set_error(error, GIMD_ERROR, GIMD_ERROR_OPERATION_FAILED,
                    "Could not get applications list from device");
        g_debug("instproxy_browse failed (%d)", err);
        goto end;
    }
    if (!apps_plist || (plist_get_node_type(apps_plist) != PLIST_ARRAY)) {
        g_set_error(error, GIMD_ERROR, GIMD_ERROR_OPERATION_FAILED,
                    "Could not parse applications list from device");
        g_debug("instproxy_browse returned an invalid plist");
        goto end;
    }

    for (i = 0; i < plist_array_get_size(apps_plist); i++) {
        plist_t app_plist = plist_array_get_item(apps_plist, i);
        GimdApplication *app;
        app = gimd_application_new_from_plist(app_plist);
        g_warn_if_fail(app != NULL);
        g_object_set(app, "device", device, NULL);
        apps = g_list_prepend(apps, app);
    }

end:
    plist_free(apps_plist);
    if (instproxy) {
        instproxy_client_free(instproxy);
    }

    return g_list_reverse(apps);
}


static void list_applications_async_thread(GTask *task,
                                           gpointer source_object,
                                           gpointer task_data,
                                           GCancellable *cancellable)
{
    GimdDevice *device = GIMD_DEVICE(source_object);
    guint flags = GPOINTER_TO_UINT(task_data);
    GList *apps;
    GError *error = NULL;

    apps = gimd_device_list_applications_sync(device, flags, &error);
    if (error != NULL) {
        g_task_return_error(task, error);
    } else {
        g_task_return_pointer(task, apps, free_gobject_list);
    }
}


/**
 * gimd_device_list_applications:
 * @device: a #GimdDevice
 * @flags: bitwise-or of GimdApplicationListFlags. If 0, only user applications
 * will be listed
 * @cancellable: optional #GCancellable object, %NULL to ignore
 * @callback: a #GAsyncReadyCallback to call when the request is satisfied
 * @user_data: the data to pass to callback function
 *
 * Gets the list of applications stored on @device. This operation is
 * asynchronous.
 */
void gimd_device_list_applications(GimdDevice *device,
                                   guint flags,
                                   GCancellable *cancellable,
                                   GAsyncReadyCallback callback,
                                   gpointer user_data)
{
    GTask *task;

    g_return_if_fail(GIMD_IS_DEVICE(device));
    g_return_if_fail((flags & ~GIMD_APPLICATION_LIST_ALL) == 0);
    g_return_if_fail((cancellable == NULL) || G_IS_CANCELLABLE(cancellable));

    task = g_task_new(device, cancellable, callback, user_data);
    g_task_set_task_data(task, GUINT_TO_POINTER(flags), NULL);
    g_task_run_in_thread(task, list_applications_async_thread);
}


/**
 * gimd_device_list_applications_finish:
 * @device: a #GimdDevice
 * @result: a #GAsyncResult
 * @error: a #GError or %NULL
 *
 * Returns: (element-type GimdApplication) (transfer full): a newly allocated list
 * of #GimdApplication
 */
GList *gimd_device_list_applications_finish(GimdDevice *device,
                                            GAsyncResult *result,
                                            GError **error)
{
    g_return_val_if_fail(GIMD_IS_DEVICE(device), NULL);
    g_return_val_if_fail((error == NULL) || (*error == NULL), NULL);

    return g_task_propagate_pointer(G_TASK(result), error);
}



/**
 * gimd_device_list_archived_applications_sync:
 *
 * @device: an #GimdDevice
 * @flags: Currently unused, must be 0.
 *
 * Gets the list of archived applications stored on @device.
 *
 * Returns: (element-type GimdApplication) (transfer full): a newly allocated list
 * of #GimdApplication
 **/
GList *gimd_device_list_archived_applications_sync(GimdDevice *device,
                                                   guint flags,
                                                   GError **error)
{
    GList *archives = NULL;
    plist_t archives_plist = NULL;
    plist_dict_iter iter = NULL;
    instproxy_client_t instproxy = NULL;
    instproxy_error_t err;

    g_return_val_if_fail(GIMD_IS_DEVICE(device), NULL);
    g_return_val_if_fail(flags == 0, NULL);

    instproxy = gimd_utils_instproxy_new(device->priv->udid);
    if (instproxy == NULL) {
        g_set_error(error, GIMD_ERROR, GIMD_ERROR_OPERATION_FAILED,
                    "Could not get archives list from device");
        goto end;
    }

    err = instproxy_lookup_archives(instproxy, NULL, &archives_plist);
    if (err != INSTPROXY_E_SUCCESS) {
        g_set_error(error, GIMD_ERROR, GIMD_ERROR_OPERATION_FAILED,
                    "Could not get archives list from device");
        g_debug("instproxy_lookup_archives failed (%d)", err);
        goto end;
    }
    if (!archives_plist || (plist_get_node_type(archives_plist) != PLIST_DICT)) {
        g_set_error(error, GIMD_ERROR, GIMD_ERROR_OPERATION_FAILED,
                    "Could not parse archives list from device");
        g_debug("instproxy_lookup_archives returned an invalid plist");
        goto end;
    }

    g_debug("got %d archived apps\n", plist_dict_get_size(archives_plist));
    plist_dict_new_iter(archives_plist, &iter);
    if (!iter) {
        g_set_error(error, GIMD_ERROR, GIMD_ERROR_OPERATION_FAILED,
                    "Could not parse archives list from device");
        g_debug("could not create plist_dict_iter");
        goto end;
    }
    do {
        GimdApplication *archive;
        plist_t archive_plist;

        plist_dict_next_item(archives_plist, iter, NULL, &archive_plist);

        if (!archive_plist) {
            break;
        }
        if (plist_get_node_type(archive_plist) != PLIST_DICT) {
            continue;
        }
        archive = gimd_application_new_from_plist(archive_plist);
        g_warn_if_fail(archive != NULL);
        g_object_set(archive, "device", device, NULL);
        archives = g_list_prepend(archives, archive);
    } while (TRUE);

end:
    free(iter);
    plist_free(archives_plist);
    if (instproxy) {
        instproxy_client_free(instproxy);
    }

    return g_list_reverse(archives);
}


static void list_archived_applications_async_thread(GTask *task,
                                                    gpointer source_object,
                                                    gpointer task_data,
                                                    GCancellable *cancellable)
{
    GimdDevice *device = GIMD_DEVICE(source_object);
    guint flags = GPOINTER_TO_UINT(task_data);
    GList *archived_apps;
    GError *error = NULL;

    archived_apps = gimd_device_list_archived_applications_sync(device,
                                                                flags,
                                                                &error);
    if (error != NULL) {
        g_task_return_error(task, error);
    } else {
        g_task_return_pointer(task, archived_apps, free_gobject_list);
    }
}


/**
 * gimd_device_list_archived_applications:
 * @device: a #GimdDevice
 * @flags: Currently unused, must be 0.
 * @cancellable: optional #GCancellable object, %NULL to ignore
 * @callback: a #GAsyncReadyCallback to call when the request is satisfied
 * @user_data: the data to pass to callback function
 *
 * Gets the list of archived applications stored on @device. This operation
 * is asynchronous.
 */
void gimd_device_list_archived_applications(GimdDevice *device,
                                            guint flags,
                                            GCancellable *cancellable,
                                            GAsyncReadyCallback callback,
                                            gpointer user_data)
{
    GTask *task;

    g_return_if_fail(GIMD_IS_DEVICE(device));
    g_return_if_fail(flags == 0);
    g_return_if_fail((cancellable == NULL) || G_IS_CANCELLABLE(cancellable));

    task = g_task_new(device, cancellable, callback, user_data);
    g_task_set_task_data(task, GUINT_TO_POINTER(flags), NULL);
    g_task_run_in_thread(task, list_archived_applications_async_thread);
}


/**
 * gimd_device_list_archived_applications_finish:
 * @device: a #GimdDevice
 * @result: a #GAsyncResult
 * @error: a #GError or %NULL
 *
 * Returns: (element-type GimdApplication) (transfer full): a newly allocated list
 * of #GimdApplication
 */
GList *gimd_device_list_archived_applications_finish(GimdDevice *device,
                                                     GAsyncResult *result,
                                                     GError **error)
{
    g_return_val_if_fail(GIMD_IS_DEVICE(device), NULL);
    g_return_val_if_fail((error == NULL) || (*error == NULL), NULL);

    return g_task_propagate_pointer(G_TASK(result), error);
}


typedef gboolean (*FileMatch)(const char *filename, gpointer user_data);

static GArray *lookup_file_data_from_archive(const char *archive_path,
                                             FileMatch file_match_cb,
                                             gpointer user_data)
{
    struct archive *archive;
    struct archive_entry *entry;
    int status;
    GArray *data = NULL;
    guint64 size;
    ssize_t bytes_read;

    archive = archive_read_new();
    archive_read_support_filter_all(archive);
    archive_read_support_format_all(archive);
    status = archive_read_open_filename(archive, archive_path, 16*1024);
    if (status != ARCHIVE_OK) {
        return NULL;
    }
    while (archive_read_next_header(archive, &entry) == ARCHIVE_OK) {
        if (!(archive_entry_filetype(entry) & AE_IFREG)) {
            archive_read_data_skip(archive);
            continue;
        }
        if (!file_match_cb(archive_entry_pathname(entry), user_data)) {
            archive_read_data_skip(archive);
            continue;
        }
        /* Found the file we are looking for, read it now */
        if (archive_entry_size_is_set(entry)) {
            size = archive_entry_size(entry);
        } else {
            size = 16*1024;
        }
        data = g_array_new(FALSE, FALSE, sizeof(guint8));
        g_array_set_size(data, size);
        bytes_read = archive_read_data(archive, data->data, data->len);
        /* FIXME: need to loop over archive_read_data */
        g_assert(bytes_read == size);
        break;
    }

    status = archive_read_free(archive);

    return data;
}


static plist_t g_array_to_plist(GArray *data)
{
    plist_t plist = NULL;

    if (data == NULL) {
        return NULL;
    }
    if (data->len < 8) {
        return NULL;
    }

    if (memcmp(data->data, "bplist00", 8) == 0) {
        plist_from_bin(data->data, data->len, &plist);
    } else {
        plist_from_xml(data->data, data->len, &plist);
    }

    return plist;
}


static GArray *lookup_data_from_archive(GFile *bundle,
                                        FileMatch file_match_cb,
                                        gpointer user_data)
{
    GArray *data;
    char *bundle_path;

    bundle_path = g_file_get_path(bundle);
    if (bundle_path == NULL) {
        /* FIXME: failed to get local file path, should be possible to use
         * archive_read_open2 and g_input_stream_read_all instead */
        return NULL;
    }

    data = lookup_file_data_from_archive(bundle_path,
                                         file_match_cb,
                                         user_data);
    g_free(bundle_path);

    return data;
}


static plist_t lookup_plist_from_archive(GFile *bundle,
                                         FileMatch file_match_cb,
                                         gpointer user_data)
{
    GArray *data;
    plist_t plist;

    data = lookup_data_from_archive(bundle, file_match_cb, user_data);
    plist = g_array_to_plist(data);
    g_array_unref(data);

    return plist;
}


static gboolean match_info_plist(const char *filename, gpointer user_data)
{
    if (!g_str_has_prefix(filename, "Payload")) {
        return FALSE;
    }
    if (!g_str_has_suffix(filename, "Info.plist")) {
        return FALSE;
    }
    return TRUE;
}


static plist_t *get_info_plist_from_bundle(GFile *bundle)
{
    return lookup_plist_from_archive(bundle, match_info_plist, NULL);
}


static plist_t get_data_from_bundle(GFile *bundle, const char *filename)
{
    return lookup_data_from_archive(bundle,
                                    (FileMatch)g_str_equal,
                                    (gpointer)filename);
}


static plist_t get_sinf_plist(GFile *app_bundle, plist_t info)
{
    char *sinf_name = NULL;
    char *bundle_executable;
    GArray *sinf_data;
    plist_t sinf;

    bundle_executable = gimd_utils_get_plist_dict_string(info,
                                                         "CFBundleExecutable");
    if (bundle_executable == NULL) {
        return NULL;
    }
    sinf_name = g_strdup_printf("Payload/%s.app/SC_Info/%s.sinf",
                                bundle_executable, bundle_executable);
    g_free(bundle_executable);

    sinf_data = get_data_from_bundle(app_bundle, sinf_name);;
    g_free(sinf_name);
    sinf = plist_new_data(sinf_data->data, sinf_data->len);
    g_array_unref(sinf_data);

    return sinf;
}


static gboolean gimd_device_copy_file(GimdDevice *device,
                                      GFile *source,
                                      const char *dest_rel_path,
                                      GError **error)
{
    GFile *destination;
    gboolean copy_succeeded;
    gchar *path;

    path = g_file_get_parse_name(source);
    g_debug("Copying '%s' to '%s' on device %s",
            path, dest_rel_path, device->priv->udid);
    g_free(path);

    destination = gimd_device_get_gfile(device, dest_rel_path);
    copy_succeeded = g_file_copy(source, destination,
                                 G_FILE_COPY_NONE, NULL,
                                 NULL, NULL,
                                 error);

    g_object_unref(destination);

    return copy_succeeded;
}


static plist_t gimd_device_get_client_opts(GFile *app_bundle, GError **error)
{
    plist_t client_opts = NULL;
    GArray *meta_data;
    plist_t meta = NULL;
    plist_t info = NULL;
    plist_t sinf = NULL;
    char *bundle_identifier = NULL;

    client_opts = instproxy_client_options_new();
    meta_data = get_data_from_bundle(app_bundle, "iTunesMetadata.plist");
    if (meta_data == NULL) {
        g_set_error(error, GIMD_ERROR, GIMD_ERROR_OPERATION_FAILED,
                    "Could not parse iTunesMetada.plist from application bundle");
        goto error;
    }
    meta = plist_new_data(meta_data->data, meta_data->len);
    g_array_unref(meta_data);
    g_debug("iTunesMetadata.plist parsed: %p", meta);
    if (meta != NULL) {
        instproxy_client_options_add(client_opts, "iTunesMetadata", meta, NULL);
    }

    info = get_info_plist_from_bundle(app_bundle);
    g_debug("Info.plist parsed: %p", info);
    if (info == NULL) {
        g_set_error(error, GIMD_ERROR, GIMD_ERROR_OPERATION_FAILED,
                    "Could not parse Info.plist from application bundle");
        goto error;
    }

    bundle_identifier = gimd_utils_get_plist_dict_string(info,
                                                         "CFBundleIdentifier");
    if (bundle_identifier != NULL) {
        g_debug("CFBundleIdentifier: %s", bundle_identifier);
        instproxy_client_options_add(client_opts, "CFBundleIdentifier", bundle_identifier, NULL);
    }

    sinf = get_sinf_plist(app_bundle, info);
    g_debug("%s.sinf parsed: %p", bundle_identifier, sinf);
    if (sinf) {
        instproxy_client_options_add(client_opts, "ApplicationSINF", sinf, NULL);
    }

cleanup:
    plist_free(info);
    plist_free(meta);
    plist_free(sinf);
    g_free(bundle_identifier);
    return client_opts;

error:
    plist_free(client_opts);
    client_opts = NULL;
    goto cleanup;
}


static gboolean gimd_device_install_application_real(GimdDevice *device,
                                                     GFile *app_bundle,
                                                     gboolean is_upgrade,
                                                     GError **error)
{
    plist_t client_opts = NULL;
    char *appid = NULL;
    char *pkg_path = NULL;
    gboolean success = FALSE;
    GimdApplication *fake_app = NULL;

    client_opts = gimd_device_get_client_opts(app_bundle, error);
    if (client_opts == NULL) {
        g_warn_if_fail((error == NULL) || (*error != NULL));
        return FALSE;
    }
    appid = gimd_utils_get_plist_dict_string(client_opts,
                                             "CFBundleIdentifier");
    if (appid == NULL) {
        g_set_error(error, GIMD_ERROR, GIMD_ERROR_OPERATION_FAILED,
                    "Could not extract application id from application bundle");
        success = FALSE;
        goto cleanup;
    }

    /* Actual installation */
    pkg_path = g_strdup_printf("PublicStaging/%s", appid);
    gimd_device_copy_file(device, app_bundle, pkg_path, error);

    /* Beware, fake application ID including PublicStaging/
     * Without this prefix, the device will fail to locate the application
     * bundle to use for installation
     */
    fake_app = gimd_application_new(pkg_path);
    g_object_set(G_OBJECT(fake_app), "device", device, NULL);
    if (is_upgrade) {
        success = gimd_application_upgrade_sync(fake_app, client_opts, error);
    } else {
        success = gimd_application_install_sync(fake_app, client_opts, error);
    }

cleanup:
    if (fake_app != NULL) {
        g_object_unref(fake_app);
    }
    plist_free(client_opts);
    g_free(pkg_path);
    g_free(appid);

    return success;
}


typedef struct {
    GTask *main_task; /* do not own a reference on the GTask */

    /* initial closure */
    gboolean is_upgrade;
    GFile *app_bundle;

    /* intermediary results */
    plist_t client_opts;
    char *appid;

    /* user provided callbacks/data */
    GimdProgressCallback progress_callback;
    gpointer progress_callback_data;
} GimdDeviceInstallAppAsyncClosure;


static void gimd_device_install_app_async_closure_free(gpointer data)
{
    GimdDeviceInstallAppAsyncClosure *closure = data;

    g_object_unref(G_OBJECT(closure->app_bundle));
    plist_free(closure->client_opts);
    g_free(closure->appid);

    g_free(closure);
}


static void install_upgrade_async_cb(GObject *source_object,
                                     GAsyncResult *result,
                                     gpointer user_data)
{
    GimdDeviceInstallAppAsyncClosure *closure = user_data;
    GimdApplication *application = GIMD_APPLICATION(source_object);
    gboolean success;
    GError *error = NULL;

    if (closure->is_upgrade) {
        success = gimd_application_upgrade_finish(application, result, &error);
    } else {
        success = gimd_application_install_finish(application, result, &error);
    }

    if (error != NULL) {
        g_task_return_error(closure->main_task, error);
    } else {
        g_task_return_boolean(closure->main_task, success);
    }
    g_object_unref(closure->main_task);
}


static void copy_app_bundle_async_cb(GObject *source_object,
                                     GAsyncResult *result,
                                     gpointer user_data)
{
    GError *error = NULL;
    gboolean copy_succeeded = FALSE;
    GimdDeviceInstallAppAsyncClosure *closure = user_data;

    g_return_if_fail(closure != NULL);
    g_return_if_fail(G_IS_FILE(closure->app_bundle));

    copy_succeeded = g_file_copy_finish(G_FILE(source_object), result, &error);
    if (error != NULL) {
        g_task_return_error(closure->main_task, error);
        g_object_unref(closure->main_task);
    } else if (!copy_succeeded) {
        g_task_return_new_error(closure->main_task,
                                GIMD_ERROR,
                                GIMD_ERROR_OPERATION_FAILED,
                                "Could not copy application bundle to device");
        g_object_unref(closure->main_task);
    } else {
        /* copy was ok */
        GimdApplication *fake_app;
        GimdDevice *device;
        char *pkg_path;

        device = GIMD_DEVICE(g_task_get_source_object(closure->main_task));

        /* FIXME: appid needs to include PublicStaging/ */
        pkg_path = g_strdup_printf("PublicStaging/%s", closure->appid);
        fake_app = gimd_application_new(pkg_path);
        g_free(pkg_path);
        g_object_set(G_OBJECT(fake_app), "device", device, NULL);
        if (closure->is_upgrade) {
            gimd_application_upgrade(fake_app,
                                     closure->client_opts,
                                     /* cancellable */NULL,
                                     closure->progress_callback,
                                     closure->progress_callback_data,
                                     install_upgrade_async_cb,
                                     closure);
        } else {
            gimd_application_install(fake_app,
                                     closure->client_opts,
                                     /* cancellable */NULL,
                                     closure->progress_callback,
                                     closure->progress_callback_data,
                                     install_upgrade_async_cb,
                                     closure);
        }
        g_object_unref(fake_app);
    }
}


static void gimd_device_copy_app_bundle_async(GimdDevice *device,
                                              GFile *source,
                                              const char *dest_rel_path,
                                              GCancellable *cancellable,
                                              gpointer closure)
{
    GFile *destination;
    gchar *path;
    gchar *uri;

    path = g_file_get_parse_name(source);
    g_debug("Copying '%s' to '%s' on device %s",
            path, dest_rel_path, device->priv->udid);
    g_free(path);

    uri = g_strdup_printf("afc://%s/%s", device->priv->udid, dest_rel_path);
    destination = g_file_new_for_uri(uri);
    g_free(uri);

    g_file_copy_async(source, destination,
                      G_FILE_COPY_NONE, G_PRIORITY_DEFAULT,
                      cancellable,
                      /* FIXME: report progress to user? */
                      NULL, NULL,
                      copy_app_bundle_async_cb, closure);
    g_object_unref(destination);
}


static void get_install_plist_async_cb(GObject *source_object,
                                       GAsyncResult *result,
                                       gpointer user_data)
{
    GimdDevice *device = GIMD_DEVICE(source_object);
    GTask *task = G_TASK(result);
    GError *error = NULL;
    GimdDeviceInstallAppAsyncClosure *closure;
    char *pkg_path;

    closure = g_task_get_task_data(task);
    g_return_if_fail(closure != NULL);
    g_return_if_fail(G_IS_FILE(closure->app_bundle));

    closure->client_opts = g_task_propagate_pointer(task, &error);
    if (error != NULL) {
        g_task_return_error(closure->main_task, error);
        g_object_unref(closure->main_task);
        return;
    }

    closure->appid = gimd_utils_get_plist_dict_string(closure->client_opts,
                                                      "CFBundleIdentifier");
    gimd_utils_dump_plist(closure->client_opts);
    if (closure->appid == NULL) {
        g_task_return_new_error(closure->main_task,
                                GIMD_ERROR, GIMD_ERROR_OPERATION_FAILED,
                                "Could not extract application id from application bundle");
        g_object_unref(closure->main_task);
        return;
    }

    /* Actual installation */
    pkg_path = g_strdup_printf("PublicStaging/%s", closure->appid);
    gimd_device_copy_app_bundle_async(device, closure->app_bundle, pkg_path,
                                      g_task_get_cancellable(task), closure);
    g_free(pkg_path);
}


static void get_install_plist_async_thread(GTask *task,
                                           gpointer source_object,
                                           gpointer task_data,
                                           GCancellable *cancellable)
{
    GError *error = NULL;
    plist_t client_opts;
    GimdDeviceInstallAppAsyncClosure *closure;

    closure = g_task_get_task_data(task);
    g_return_if_fail(closure != NULL);
    g_return_if_fail(G_IS_FILE(closure->app_bundle));

    client_opts = gimd_device_get_client_opts(closure->app_bundle, &error);
    if (error != NULL) {
        g_task_return_error(task, error);
    } else {
        g_task_return_pointer(task, client_opts, plist_free);
    }
}


void gimd_device_install_application(GimdDevice *device,
                                     GFile *bundle,
                                     GCancellable *cancellable,
                                     GimdProgressCallback progress_callback,
                                     gpointer progress_callback_data,
                                     GAsyncReadyCallback callback,
                                     gpointer user_data)
{
    GimdDeviceInstallAppAsyncClosure *closure;
    GTask *task;

    g_return_val_if_fail(GIMD_IS_DEVICE(device), FALSE);
    g_return_val_if_fail(G_IS_FILE(bundle), FALSE);

    closure = g_new0(GimdDeviceInstallAppAsyncClosure, 1);
    closure->app_bundle = g_object_ref(bundle);
    closure->progress_callback = progress_callback;
    closure->progress_callback_data = progress_callback_data;
    closure->main_task = g_task_new(device, cancellable, callback, user_data);
    g_task_set_task_data(closure->main_task, closure,
                         gimd_device_install_app_async_closure_free);

    task = g_task_new(device, cancellable, get_install_plist_async_cb, NULL);
    g_task_set_task_data(task, closure, NULL);
    g_task_run_in_thread(task, get_install_plist_async_thread);
    g_object_unref(task);
}


gboolean gimd_device_install_application_finish(GimdDevice *device,
                                                GAsyncResult *result,
                                                GError **error)
{
    g_return_val_if_fail(GIMD_IS_DEVICE(device), FALSE);
    g_return_val_if_fail((error == NULL) || (*error == NULL), FALSE);

    return g_task_propagate_boolean(G_TASK(result), error);
}


gboolean gimd_device_install_application_sync(GimdDevice *device,
                                              GFile *bundle,
                                              GError **error)
{
    g_return_val_if_fail(GIMD_IS_DEVICE(device), FALSE);
    g_return_val_if_fail(G_IS_FILE(bundle), FALSE);
    g_return_val_if_fail((error == NULL) || (*error == NULL), FALSE);

    return gimd_device_install_application_real(device, bundle, FALSE, error);
    /* For .ipcc file: */
    /* make sure dir PublicStaging exists on device */
    /* create PublicStaging/$appid on device */
    /* extract .ipcc file there (it's a .zip) */
    /* instproxy_client_options_add(client_opts, "PackageType", "CarrierBundle", NULL); */

    /* For directories: */
    /* instproxy_client_options_add(client_opts, "PackageType", "Developer", NULL); */
    /* copy directory to device */

/* lookup iTunesMetadata.plist in archive -> meta.plist
 * lookup Payload/ .* /Info.plist in archive -> info.plist
 * get CFBundleExecutable from info.plist
 * get CFBundleIdentifier from info.plist
 * try to read Payload/$CFBundleExecutable.app/SC_Info/$CFBundleExecutable.sinf
 *   -> sinf.plist
 *
 * copy archive to PublicStaging/$CFBundleIdentifier
 * instproxy_client_options_add(client_opts, "CFBundleIdentifier", bundleidentifier);
 * instproxy_client_options_add(client_opts, "ApplicationSINF", sinf.plist, NULL):
 * instproxy_client_options_add(client_opts, "iTunesMetadata", meta.plist, NULL);
 *
 * instproxy_install/instproxy_upgrade
 */
}


void gimd_device_upgrade_application(GimdDevice *device,
                                     GFile *bundle,
                                     GCancellable *cancellable,
                                     GimdProgressCallback progress_callback,
                                     gpointer progress_callback_data,
                                     GAsyncReadyCallback callback,
                                     gpointer user_data)
{

}


gboolean gimd_device_upgrade_application_finish(GimdDevice *device,
                                                GAsyncResult *result,
                                                GError **error)
{
    g_return_val_if_fail(GIMD_IS_DEVICE(device), FALSE);
    g_return_val_if_fail((error == NULL) || (*error == NULL), FALSE);

    return g_task_propagate_boolean(G_TASK(result), error);
}


gboolean gimd_device_upgrade_application_sync(GimdDevice *device,
                                              GFile *bundle,
                                              GError **error)
{
    g_return_val_if_fail(GIMD_IS_DEVICE(device), FALSE);
    g_return_val_if_fail(G_IS_FILE(bundle), FALSE);
    g_return_val_if_fail((error == NULL) || (*error == NULL), FALSE);

    return gimd_device_install_application_real(device, bundle, TRUE, error);
}


/**
 * gimd_device_get_gvolume:
 * @device: a #GimdDevice
 *
 * Returns: (transfer full):
 **/
GVolume *gimd_device_get_gvolume(GimdDevice *device)
{
    GVolumeMonitor *monitor;
    GVolume *volume;

    g_return_val_if_fail(GIMD_IS_DEVICE(device), NULL);
    g_return_val_if_fail(device->priv->udid != NULL, NULL);

    monitor = g_volume_monitor_get();
    volume = g_volume_monitor_get_volume_for_uuid(monitor,
                                                  device->priv->udid);
    g_object_unref(monitor);

    return volume;
}


/**
 * gimd_device_get_gfile:
 * @device: a #GimdDevice
 * @rel_path: a relative path on @device
 *
 * Returns: (transfer full): a GFile to access the location at @rel_path on the
 * device.
 **/
GFile *gimd_device_get_gfile(GimdDevice *device, const char *rel_path)
{
    GFile *gfile;
    char *uri;

    g_return_val_if_fail(GIMD_IS_DEVICE(device), NULL);
    g_return_val_if_fail(device->priv->udid != NULL, NULL);
    g_return_val_if_fail(rel_path != NULL, NULL);

    uri = g_strdup_printf("afc://%s/%s", device->priv->udid, rel_path);
    gfile = g_file_new_for_uri(uri);
    g_free(uri);

    return gfile;
}


const GStrv gimd_device_get_domains(GimdDevice *device)
{
    static const char *domains[] = {
        "com.apple.disk_usage",
        "com.apple.disk_usage.factory",
        "com.apple.mobile.battery",
        /* FIXME: For some reason lockdownd segfaults on this, works sometimes though
           "com.apple.mobile.debug",. */
        "com.apple.iqagent",
        "com.apple.purplebuddy",
        "com.apple.PurpleBuddy",
        "com.apple.mobile.chaperone",
        "com.apple.mobile.third_party_termination",
        "com.apple.mobile.lockdownd",
        "com.apple.mobile.lockdown_cache",
        "com.apple.xcode.developerdomain",
        "com.apple.international",
        "com.apple.mobile.data_sync",
        "com.apple.mobile.tethered_sync",
        "com.apple.mobile.mobile_application_usage",
        "com.apple.mobile.backup",
        "com.apple.mobile.nikita",
        "com.apple.mobile.restriction",
        "com.apple.mobile.user_preferences",
        "com.apple.mobile.sync_data_class",
        "com.apple.mobile.software_behavior",
        "com.apple.mobile.iTunes.SQLMusicLibraryPostProcessCommands",
        "com.apple.mobile.iTunes.accessories",
        "com.apple.mobile.internal", /**< iOS 4.0+ */
        "com.apple.mobile.wireless_lockdown", /**< iOS 4.0+ */
        "com.apple.fairplay",
        "com.apple.iTunes",
        "com.apple.mobile.iTunes.store",
        "com.apple.mobile.iTunes",
        NULL
    };

    return (GStrv)domains;
}


plist_t gimd_device_lookup(GimdDevice *device,
                           const char *domain,
                           const char *key)
{
    lockdownd_client_t client = NULL;
    lockdownd_error_t lerr;
    plist_t node = NULL;

    client = gimd_utils_lockdownd_new(device->priv->udid);
    if (client == NULL) {
        return NULL;
    }
    lerr = lockdownd_get_value(client, domain, key, &node);
    if (lerr != LOCKDOWN_E_SUCCESS) {
        g_warn_if_fail(node == NULL);
    }
    lockdownd_client_free(client);

    return node;
}
