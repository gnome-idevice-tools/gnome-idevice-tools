/*
 * gimd-application-priv.h: class representing an application
 *
 * Copyright (C) 2012-2014 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Author: Christophe Fergeau <cfergeau@redhat.com>
 */
#ifndef __GIMD_APPLICATION_PRIV_H__
#define __GIMD_APPLICATION_PRIV_H__

#include <glib-object.h>
#include <gio/gio.h>
#include <libimobiledevice/installation_proxy.h>
#include <plist/plist.h>

G_GNUC_INTERNAL void gimd_application_upgrade(GimdApplication *application,
                                              plist_t client_opts,
                                              GCancellable *cancellable,
                                              GimdProgressCallback progress_callback,
                                              gpointer progress_callback_data,
                                              GAsyncReadyCallback callback,
                                              gpointer user_data);
G_GNUC_INTERNAL gboolean gimd_application_upgrade_finish(GimdApplication *application,
                                                         GAsyncResult *result,
                                                         GError **error);
G_GNUC_INTERNAL gboolean gimd_application_upgrade_sync(GimdApplication *application,
                                                       plist_t client_opts,
                                                       GError **error);

G_GNUC_INTERNAL void gimd_application_install(GimdApplication *application,
                                              plist_t client_opts,
                                              GCancellable *cancellable,
                                              GimdProgressCallback progress_callback,
                                              gpointer progress_callback_data,
                                              GAsyncReadyCallback callback,
                                              gpointer user_data);
G_GNUC_INTERNAL gboolean gimd_application_install_finish(GimdApplication *application,
                                                         GAsyncResult *result,
                                                         GError **error);
G_GNUC_INTERNAL gboolean gimd_application_install_sync(GimdApplication *application,
                                                       plist_t client_opts,
                                                       GError **error);
#endif
