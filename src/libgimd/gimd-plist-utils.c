/*
 * gimd-plist-utils.c: libplist helpers
 *
 * Copyright (C) 2014 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Author: Christophe Fergeau <cfergeau@redhat.com>
 */
#include <config.h>

#include <stdlib.h>

#include "gimd-plist-utils.h"

char *gimd_utils_get_plist_dict_string(plist_t dict, const char *key)
{
    plist_t item;
    char *value;

    item = plist_dict_get_item(dict, key);
    if (item == NULL) {
        return NULL;
    }

    plist_get_string_val(item, &value);
    if (!g_mem_is_system_malloc()) {
        /* Generate a string which can be freed with g_free() */
        char *copy;
        copy = g_strdup(value);
        free(value);
        value = copy;
    }

    return value;
}


guint64 gimd_utils_get_plist_dict_uint64(plist_t dict,
                                         const char *key,
                                         gboolean *found)
{
    plist_t item;
    guint64 value;

    if (found != NULL) {
        *found = FALSE;
    }
    item = plist_dict_get_item(dict, key);
    if (item == NULL) {
        return 0;
    }

    plist_get_uint_val(item, &value);
    if (found != NULL) {
        *found = TRUE;
    }

    return value;
}


void gimd_utils_dump_plist(plist_t plist)
{
    char *xml = NULL;
    uint32_t size = 0;

    plist_to_xml(plist, &xml, &size);
    g_print("client options: %s\n", xml);
    free(xml);
}
